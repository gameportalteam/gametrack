'use strict';

module.exports = {
  up: function up(queryInterface, Sequelize) {
    return queryInterface.addColumn('Games', 'link', Sequelize.STRING)
      .then(() => queryInterface.addColumn('Games', 'logourl', Sequelize.STRING))
      .then(() => queryInterface.addColumn('Games', 'description', Sequelize.TEXT));
  },
  down: function down(queryInterface) {
    return queryInterface.removeColumn('Games', 'link')
      .then(() => queryInterface.removeColumn('Games', 'logourl'))
      .then(() => queryInterface.removeColumn('Games', 'description'));
  }
};

'use strict';

const users = require('../../../server/config/data/users.json').users;
const profiles = require('../../../server/config/data/gameProfiles.json').profiles;
const games = require('../../../server/config/data/games.json').games;
const communities = require('../../../server/config/data/communities.json').communities;
const reports = require('../../../server/config/data/reports-list.json').reports;
const utils = require('../utils.js');
const insertEntities = utils.insertEntities;
const insertAccounts = utils.insertAccounts;
const insertGames = utils.insertGames;
const insertKarma = utils.insertKarma;
const insertProfiles = utils.insertProfiles;
const insertCommunities = utils.insertCommunities;
const insertReports = utils.insertReports;
const insertKarmaCoeffs = utils.insertKarmaCoeffs;
const fixautoid = utils.fixautoid;

module.exports = {
  up: function up(queryInterface) {
    return insertEntities(queryInterface, users.length, 'account')
      .then(() => insertAccounts(queryInterface, users))
      .then(() => insertEntities(queryInterface, games.length, 'game'))
      .then(() => insertGames(queryInterface, games))
      .then(() => insertEntities(queryInterface, profiles.length, 'gameProfiles'))
      .then(() => insertKarma(queryInterface, profiles.length))
      .then(() => insertProfiles(queryInterface, profiles))
      .then(() => insertEntities(queryInterface, communities.length, 'community'))
      .then(() => insertCommunities(queryInterface, communities))
      .then(() => insertReports(queryInterface, reports))
      .then(() => insertKarmaCoeffs(queryInterface))
      .then(() => fixautoid(queryInterface));
  },
  down: function down() {
  }
};

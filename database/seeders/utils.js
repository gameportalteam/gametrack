'use strict';

let entityId = 1;
let id = 1;
let karmaId = 1;

function insertAccounts(queryInterface, users) {
  const promises = [];
  users.forEach(user => {
    promises.push(queryInterface.bulkInsert('Accounts', [{
      id: user.id,
      name: user.name,
      avatarurl: user.avatarurl,
      email: user.email,
      password: user.password,
      salt: user.salt,
      entityId: entityId++,
      entityType: user.entityType,
      active: user.active,
      createdAt: new Date(),
      updatedAt: new Date(),
      role: user.role
    }]));
  });
  return promises;
}

function insertProfiles(queryInterface, profiles) {
  const promises = [];
  profiles.forEach(profile => {
    promises.push(queryInterface.bulkInsert('GameProfiles', [{
      id: profile.id,
      nickname: profile.nickname,
      accountId: profile.accountId,
      entityId: entityId++,
      entityType: profile.entityType,
      removed: profile.removed,
      gameId: profile.gameId,
      isPrimary: profile.isPrimary,
      karmaId: profile.id,
      createdAt: new Date(),
      updatedAt: new Date()
    }]));
  });
  return promises;
}

function insertEntities(queryInterface, count, type) {
  const promises = [];
  for (let i = 0; i < count; i++) {
    promises.push(queryInterface.bulkInsert('Entities', [
      { id: id++, type: type, createdAt: new Date(), updatedAt: new Date() }]));
  }
  return Promise.all(promises);
}

function insertKarma(queryInterface, count) {
  const promises = [];
  for (let i = 0; i < count; i++) {
    promises.push(queryInterface.bulkInsert('Karmas', [{
      id: karmaId++,
      likePoints: 0,
      reportPoints: 0,
      commentPoints: 0,
      likeCount: 0,
      reportCount: 0,
      commentCount: 0,
      createdAt: new Date(),
      updatedAt: new Date()
    }]));
  }
  return Promise.all(promises);
}

function insertGames(queryInterface, games) {
  const promises = [];
  let i = 1;
  games.forEach(game => {
    promises.push(queryInterface.bulkInsert('Games', [{
      id: i++,
      title: game.title,
      entityId: entityId++,
      entityType: game.entityType,
      createdAt: new Date(),
      updatedAt: new Date()
    }]));
  });
  return Promise.all(promises);
}

function insertCommunities(queryInterface, communities) {
  const promises = [];
  let i = 1;
  communities.forEach(community => {
    promises.push(queryInterface.bulkInsert('Communities', [{
      id: i++,
      title: community.title,
      entityId: entityId++,
      entityType: community.entityType,
      gameId: community.gameId,
      parentId: community.parentId,
      removed: community.removed,
      creatorId: community.creatorId,
      active: community.active,
      createdAt: new Date(),
      updatedAt: new Date()
    }]));
  });
  return Promise.all(promises);
}

function insertReports(queryInterface, reports) {
  const promises = [];
  let i = 1;
  reports.forEach(report => {
    promises.push(queryInterface.bulkInsert('ReportLists', [{
      id: i++,
      title: report.title,
      isPositive: report.isPositive,
      createdAt: new Date(),
      updatedAt: new Date()
    }]));
  });
  return Promise.all(promises);
}

function insertKarmaCoeffs(queryInterface) {
  return queryInterface.bulkInsert('KarmaCoeffs', [{
    id: 1,
    likeCount: 0,
    reportCount: 0,
    commentCount: 0,
    defaultPoints: 0.001,
    diffCoeff: 1000,
    createdAt: new Date(),
    updatedAt: new Date()
  }]);
}

function fixautoid(queryInterface) {
  return queryInterface.sequelize.query(`
  SELECT setval('"Accounts_id_seq"'::regclass, 2);
  SELECT setval('"Entities_id_seq"'::regclass, 13);
  SELECT setval('"GameProfiles_id_seq"'::regclass, 3);
  SELECT setval('"Games_id_seq"'::regclass, 3);
  SELECT setval('"Communities_id_seq"'::regclass, 5);
  SELECT setval('"ReportLists_id_seq"'::regclass, 6);
  SELECT setval('"Karmas_id_seq"'::regclass, 3);
  `);
}

module.exports = {
  insertEntities: insertEntities,
  insertReports: insertReports,
  insertCommunities: insertCommunities,
  insertGames: insertGames,
  insertKarma: insertKarma,
  insertAccounts: insertAccounts,
  insertProfiles: insertProfiles,
  insertKarmaCoeffs: insertKarmaCoeffs,
  fixautoid: fixautoid
};

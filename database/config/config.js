const baseConfig = require('../../server/config/db');
const _ = require('lodash');

const config = _.merge(baseConfig, {
  dialect: 'postgres',
  seederStorage: 'sequelize'
});

module.exports = {
  development: config,
  production: config
};

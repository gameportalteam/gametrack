'use strict';

import sequelize from '../../server/config/databaseConnection.js';
import Sequelize from 'sequelize';

const CommentSchema = {
  entityId: {
    type: Sequelize.INTEGER,
    allowNull: false
  },
  rootId: {
    type: Sequelize.INTEGER,
    allowNull: false
  },
  targetEntityId: {
    type: Sequelize.INTEGER,
    allowNull: false
  },
  path: {
    type: Sequelize.STRING,
    allowNull: false
  },
  whoId: {
    type: Sequelize.INTEGER,
    allowNull: false
  },
  entityType: {
    type: Sequelize.STRING,
    defaultValue: 'comment'
  },
  text: {
    type: Sequelize.TEXT,
    allowNull: false
  }
};

function toDto() {
  const comment = this.dataValues;
  return {
    id: comment.id,
    path: comment.path,
    authorName: comment.authorName || `id${comment.authorId}`,
    authorId: comment.authorId,
    gameId: comment.gameId,
    entityId: comment.entityId,
    rootId: comment.rootId,
    targetEntityId: comment.targetEntityId,
    text: comment.text,
    created: comment.createdAt
  };
}

export default sequelize.define('Comment', CommentSchema, {
  instanceMethods: { toDto: toDto }
});

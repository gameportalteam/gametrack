'use strict';

import sequelize from '../../server/config/databaseConnection.js';
import Sequelize from 'sequelize';

const ReportCommunitySchema = {
  fromId: {
    type: Sequelize.INTEGER,
    allowNull: false
  },
  toId: {
    type: Sequelize.INTEGER,
    allowNull: false
  },
  reportId: {
    type: Sequelize.INTEGER,
    allowNull: false
  },
  entityType: {
    type: Sequelize.STRING,
    allowNull: false,
    defaultValue: 'report'
  }
};

function toDto() {
  return {
    id: this.id,
    to: this.to.toDto(),
    from: this.from.toDto(),
    report: this.report.toDto(),
    entityType: this.entityType
  };
}

export default sequelize.define('ReportCommunity', ReportCommunitySchema, {
  instanceMethods: { toDto: toDto }
});

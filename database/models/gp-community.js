'use string';

import sequelize from '../../server/config/databaseConnection.js';
import Sequelize from 'sequelize';

const GameProfileCommunitySchema = {
  communityId: {
    type: Sequelize.INTEGER,
    allowNull: false
  },
  gameProfileId: {
    type: Sequelize.INTEGER,
    allowNull: false
  }
};

export default sequelize.define('GameProfileCommunity', GameProfileCommunitySchema);

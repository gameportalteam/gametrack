'use strict';

import sequelize from '../../server/config/databaseConnection.js';
import Sequelize from 'sequelize';


const LikeSchema = {
  entityType: {
    type: Sequelize.STRING,
    defaultValue: 'like'
  },
  type: {
    type: Sequelize.INTEGER,
    allowNull: false
  },
  targetEntityId: {
    type: Sequelize.INTEGER,
    allowNull: false
  },
  whoId: {
    type: Sequelize.INTEGER,
    allowNull: false
  }
};

function toDto() {
  const like = this.dataValues;
  return {
    id: like.id,
    type: like.type,
    targetEntityId: like.targetEntityId,
    whoId: like.whoId
  };
}

export default sequelize.define('Like', LikeSchema, {
  instanceMethods: {
    toDto: toDto
  }
});

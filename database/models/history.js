'use strict';

import sequelize from '../../server/config/databaseConnection.js';
import Sequelize from 'sequelize';

const HistorySchema = {
  targetEntityType: {
    type: Sequelize.STRING,
    allowNull: false
  },
  targetEntityId: {
    type: Sequelize.INTEGER,
    allowNull: false
  },
  entityOwnerId: {
    type: Sequelize.INTEGER,
    allowNull: false
  },
  sourceId: {
    type: Sequelize.INTEGER,
    allowNull: false
  },
  sourceEntityType: {
    type: Sequelize.STRING,
    allowNull: false
  },
  karmaPoints: Sequelize.FLOAT,
  action: {
    type: Sequelize.STRING,
    allowNull: false
  },
  operation: {
    type: Sequelize.STRING,
    allowNull: false
  }
};

export default sequelize.define('History', HistorySchema);

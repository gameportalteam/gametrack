'use strict';

import sequelize from '../../server/config/databaseConnection.js';
import Sequelize from 'sequelize';

export default sequelize.define('Entity', {
  type: Sequelize.STRING
});

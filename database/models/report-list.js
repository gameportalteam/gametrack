'use strict';

import sequelize from '../../server/config/databaseConnection.js';
import Sequelize from 'sequelize';

const ReportListSchema = {
  title: {
    type: Sequelize.TEXT,
    allowNull: false
  },
  isPositive: {
    type: Sequelize.BOOLEAN,
    allowNull: false
  }
};

export default sequelize.define('ReportList', ReportListSchema, {
  instanceMethods: {
    toDto() {
      return {
        id: this.id,
        title: this.title,
        isPositive: this.isPositive
      };
    }
  }
});

'use strict';

import sequelize from '../../server/config/databaseConnection.js';
import Sequelize from 'sequelize';

const GameProfileSchema = {
  nickname: {
    type: Sequelize.STRING,
    allowNull: false
  },
  accountId: {
    type: Sequelize.INTEGER,
    defaultValue: 0
  },
  entityType: {
    type: Sequelize.STRING,
    defaultValue: 'gameProfile'
  },
  entityId: {
    type: Sequelize.INTEGER,
    allowNull: false
  },
  removed: {
    type: Sequelize.BOOLEAN,
    defaultValue: false
  },
  gameId: {
    type: Sequelize.INTEGER,
    allowNull: false
  },
  isPrimary: {
    type: Sequelize.BOOLEAN,
    defaultValue: false
  },
  karmaId: {
    type: Sequelize.INTEGER,
    allowNull: false
  },
  createdAt: { 
    type: Sequelize.DATE, 
    defaultValue: Sequelize.NOW 
  },
};

function toDto() {
  const profile = {
    id: this.id,
    entityId: this.entityId,
    name: this.nickname,
    entityType: this.entityType,
    accountId: this.accountId,
    gameId: this.gameId,
    isPrimary: this.isPrimary,
    createdAt: this.createdAt,

  };
  if (this.game) profile.game = this.game.toDto();
  if (this.karma) profile.karma = this.karma.toDto();
  return profile;
}

export default sequelize.define('GameProfile', GameProfileSchema, {
  instanceMethods: { toDto: toDto }
});

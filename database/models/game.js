'use strict';

import sequelize from '../../server/config/databaseConnection.js';
import Sequelize from 'sequelize';

const GameSchema = {
  title: {
    type: Sequelize.STRING,
    allowNull: false
  },
  link: Sequelize.STRING,
  logourl: Sequelize.STRING,
  description: Sequelize.TEXT,
  entityType: {
    type: Sequelize.STRING,
    defaultValue: 'game'
  },
  entityId: {
    type: Sequelize.INTEGER,
    allowNull: false
  }
};

export default sequelize.define('Game', GameSchema, {
  instanceMethods: {
    toDto() {
      const game = this.dataValues;
      const dto = {
        id: game.id,
        gameId: game.id,
        entityType: game.entityType,
        entityId: game.entityId,
        title: game.title,
        logourl: game.logourl
      };
      return dto;
    }
  }
});

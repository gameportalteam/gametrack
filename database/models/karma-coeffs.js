'use strict';

import sequelize from '../../server/config/databaseConnection.js';
import Sequelize from 'sequelize';

const KarmaCoeffsSchema = {
  likeCount: {
    type: Sequelize.INTEGER,
    allowNull: true,
    defaultValue: 0
  },
  reportCount: {
    type: Sequelize.INTEGER,
    allowNull: true,
    defaultValue: 0
  },
  commentCount: {
    type: Sequelize.INTEGER,
    allowNull: true,
    defaultValue: 0
  },
  defaultPoints: {
    type: Sequelize.FLOAT,
    defaultValue: 0.001
  },
  diffCoeff: {
    type: Sequelize.INTEGER,
    defaultValue: 10000
  }
};

function toDto() {
  const sum = this.likePoints
    + this.commentPoints
    + this.reportPoints;
  return {
    likeCoeff: 1 - (this.likePoints / sum),
    commentCoeff: 1 - (this.commentPoints / sum),
    reportCoeff: 1 - (this.reportPoints / sum)
  };
}

export default sequelize.define('KarmaCoeffs', KarmaCoeffsSchema, {
  instanceMethods: { toDto: toDto }
});

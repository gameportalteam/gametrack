'use strict';

import sequelize from '../../server/config/databaseConnection.js';
import Sequelize from 'sequelize';

const KarmaSchema = {
  likePoints: {
    type: Sequelize.DOUBLE,
    allowNull: false,
    defaultValue: 0
  },
  reportPoints: {
    type: Sequelize.DOUBLE,
    allowNull: false,
    defaultValue: 0
  },
  commentPoints: {
    type: Sequelize.DOUBLE,
    allowNull: false,
    defaultValue: 0
  },
  likeCount: {
    type: Sequelize.INTEGER,
    allowNull: false,
    defaultValue: 0
  },
  reportCount: {
    type: Sequelize.INTEGER,
    allowNull: false,
    defaultValue: 0
  },
  commentCount: {
    type: Sequelize.INTEGER,
    allowNull: false,
    defaultValue: 0
  }
};

function toDto() {
  return (this.likePoints + this.commentPoints + this.reportPoints
    + Math.log10(this.likeCount + this.commentCount + this.reportCount + 1)).toFixed(4);
}

export default sequelize.define('Karma', KarmaSchema, {
  instanceMethods: { toDto: toDto }
});

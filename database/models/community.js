'use strict';

import sequelize from '../../server/config/databaseConnection.js';
import Sequelize from 'sequelize';

const CommunitySchema = {
  title: {
    type: Sequelize.STRING,
    allowNull: false
  },
  entityType: {
    type: Sequelize.STRING,
    defaultValue: 'community'
  },
  parentId: Sequelize.INTEGER,
  gameId: {
    type: Sequelize.INTEGER,
    allowNull: false
  },
  tag: {
    type: Sequelize.STRING
  },
  entityId: {
    type: Sequelize.INTEGER,
    allowNull: false
  },
  removed: {
    type: Sequelize.BOOLEAN,
    defaultValue: false
  },
  creatorId: {
    type: Sequelize.INTEGER,
    allowNull: false
  },
  active: {
    type: Sequelize.BOOLEAN,
    allowNull: false
  }
};

function toDto() {
  const community = this.dataValues;
  const dto = {
    id: community.id,
    entityType: community.entityType,
    entityId: community.entityId,
    parentId: community.parentId,
    title: community.title,
    tag: community.tag,
    active: community.active,
    created: community.createdAt
  };
  if (community.creator) dto.creator = community.creator.toDto();
  if (community.game) {
    dto.game = community.game.toDto();
    dto.gameId = dto.game.id;
  }
  return dto;
}

export default sequelize.define('Community', CommunitySchema, {
  instanceMethods: { toDto: toDto }
});

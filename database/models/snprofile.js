'use strict';

import sequelize from '../../server/config/databaseConnection.js';
import Sequelize from 'sequelize';

const SNProfileSchema = {
  snId: {
    type: Sequelize.STRING
  },
  snType: {
    type: Sequelize.STRING,
    allowNull: false
  },
  data: {
    type: Sequelize.JSONB
  },
  entityType: {
    type: Sequelize.STRING,
    allowNull: false,
    defaultValue: 'SNProfile'
  }
};

export default sequelize.define('SNProfile', SNProfileSchema, {
  instanceMethods: {
    toDto() {
      const profile = this.dataValues;
      const dto = profile.data;
      dto.id = profile.snId;
      dto.provider = profile.snType;
      return dto;
    }
  }
});

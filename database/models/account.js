'use strict';

import sequelize from '../../server/config/databaseConnection.js';
import Sequelize from 'sequelize';
import crypto from 'crypto';

const authTypes = ['facebook', 'vkontakte'];

const validatePresenceOf = (value) => {
  return value && value.length;
};

const AccountSchema = {
  name: {
    type: Sequelize.STRING
  },
  avatarurl: {
    type: Sequelize.STRING
  },
  password: {
    type: Sequelize.STRING,
    validate: {
      notEmpty: true
    }
  },
  salt: Sequelize.STRING,
  email: {
    type: Sequelize.STRING,
    unique: {
      msg: 'The specified email address is already in use.'
    },
    validate: {
      isEmail: true
    }
  },
  role: {
    type: Sequelize.STRING,
    allowNull: false,
    defaultValue: 'user'
  },
  entityType: {
    type: Sequelize.STRING,
    allowNull: false,
    defaultValue: 'account'
  },
  entityId: {
    type: Sequelize.INTEGER,
    allowNull: false
  },
  active: {
    type: Sequelize.BOOLEAN,
    allowNull: false,
    defaultValue: true
  }
};

export default sequelize.define('Account', AccountSchema, {

  /**
   * Virtual Getters
   */
  getterMethods: {
    // Public profile information
    profile() {
      return {
        name: this.name,
        role: this.role
      };
    },

    // Non-sensitive info we'll be putting in the token
    token() {
      return {
        id: this.id,
        role: this.role
      };
    }
  },

  /**
   * Pre-save hooks
   */
  hooks: {
    beforeBulkCreate: (users, fields, fn) => {
      let totalUpdated = 0;
      users.forEach((user) => {
        user.updatePassword((err) => {
          if (err) {
            return fn(err);
          }
          totalUpdated += 1;
          if (totalUpdated === users.length) {
            return fn();
          }
        });
      });
    },
    beforeCreate: (user, fields, fn) => {
      user.updatePassword(fn);
    },
    beforeUpdate: (user, fields, fn) => {
      if (user.changed('password')) {
        return user.updatePassword(fn);
      }
      fn();
    }
  },

  /**
   * Instance Methods
   */
  instanceMethods: {

    toDto() {
      const dto = {};
      const acc = this.dataValues;
      dto.id = acc.id;
      dto.entityType = acc.entityType;
      dto.entityId = acc.entityId;
      dto.name = acc.name || acc.id;
      dto.avatarurl = acc.avatarurl;
      dto.email = acc.email;
      dto.role = acc.role;
      dto.active = acc.active;
      if (this.snProfiles) {
        dto.snProfiles = this.snProfiles.map(x => x.toDto());
      }
      return dto;
    },

    /**
     * Authenticate - check if the passwords are the same
     *
     * @param {String} password
     * @param {Function} callback
     * @return {Boolean}
     * @api public
     */
    authenticate(password, callback) {
      if (!callback) {
        return this.password === this.encryptPassword(password);
      }
      this.encryptPassword(password, (err, pwdGen) => {
        if (err) {
          callback(err);
        }
        if (this.password === pwdGen) {
          callback(null, true);
        } else {
          callback(null, false);
        }
      });
    },

    /**
     * Make salt
     *
     * @param {Number} byteSize Optional salt byte size, default to 16
     * @param {Function} callback
     * @return {String}
     * @api public
     */
    makeSalt(byteSize, callback) {
      const defaultByteSize = 16;

      if (typeof byteSize === 'function') {
        callback = byteSize;
        byteSize = defaultByteSize;
      }

      if (!byteSize) {
        byteSize = defaultByteSize;
      }

      if (!callback) {
        return crypto.randomBytes(byteSize).toString('base64');
      }

      return crypto.randomBytes(byteSize, (err, salt) => {
        if (err) {
          callback(err);
        }
        return callback(null, salt.toString('base64'));
      });
    },

    /**
     * Encrypt password
     *
     * @param {String} password
     * @param {Function} callback
     * @return {String}
     * @api public
     */
    encryptPassword(password, callback) {
      if (!password || !this.salt) {
        if (!callback) {
          return null;
        }
        return callback(null);
      }

      const defaultIterations = 10000;
      const defaultKeyLength = 64;
      const salt = new Buffer(this.salt, 'base64');

      if (!callback) {
        return crypto.pbkdf2Sync(password, salt, defaultIterations, defaultKeyLength)
          .toString('base64');
      }

      return crypto.pbkdf2(password, salt, defaultIterations, defaultKeyLength,
        (err, key) => {
          if (err) {
            callback(err);
          } else {
            callback(null, key.toString('base64'));
          }
        });
    },

    /**
     * Update password field
     *
     * @param {Function} fn
     * @return {String}
     * @api public
     */
    updatePassword(fn) {
      // Handle new/update passwords
      if (!this.password) {
        fn(null);
      } else {
        if (!validatePresenceOf(this.password) && authTypes.indexOf(this.provider) === -1) {
          fn(new Error('Invalid password'));
        }

        // Make salt with a callback
        this.makeSalt((saltErr, salt) => {
          if (saltErr) {
            fn(saltErr);
          }
          this.salt = salt;
          this.encryptPassword(this.password, (encryptErr, hashedPassword) => {
            if (encryptErr) {
              fn(encryptErr);
            }
            this.password = hashedPassword;
            fn(null);
          });
        });
      }
    }
  }
});

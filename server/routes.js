import errors from './components/errors';
import path from 'path';
import config from './config';

import usersRoutes from './api/account';
import likesRoutes from './api/like';
import communitiesRoutes from './api/community';
import gameProfilesRoutes from './api/game-profile';
import commentsRoutes from './api/comment';
import authRoutes from './auth';
import gameRoutes from './api/game';
import reportRoutes from './api/report';
import reportListRoutes from './api/report-list';
import entityShow from './api/entityShow';
import adminRoutes from './api/admin';

export default function (app) {
  // Insert routes below

  app.use('/api/users', usersRoutes);
  app.use('/api/likes', likesRoutes);
  app.use('/api/communities', communitiesRoutes);
  app.use('/api/gameProfiles', gameProfilesRoutes);
  app.use('/api/comments', commentsRoutes);
  app.use('/api/games', gameRoutes);
  app.use('/auth', authRoutes);
  app.use('/api/reports', reportRoutes);
  app.use('/api/reportslist', reportListRoutes);
  app.use('/api/entityShow', entityShow);

  // todo: WTF? We haven't admins at GT [SV]
  app.use('/api/admin', adminRoutes);

  // All undefined asset or api routes should return a 404
  app.route('/:url(api|auth|components|app|bower_components|assets)/*')
    .get(errors[404]);

  // All other routes should redirect to the index.html
  app.route('/*')
    .get((req, res) => {
      res.sendFile(path.resolve(`${config.appPath}/index.html`));
    });
}

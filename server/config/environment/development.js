const host = 'http://localhost:9000';

import env from '../nodeEnv';

const sn = {
  facebook: {
    clientID: '990523214370000',
    clientSecret: 'd74c423a351b02ff17b61ef64d163ae9',
    callbackURL: `${host}/auth/facebook/callback`
  },

  twitter: {
    clientID: '727086713131118592',
    clientSecret: '8Fw15k5LzWRigvKurWpY4BBBJW2hCxxt5EQQStto5idTgATqJR',
    callbackURL: `${host}/auth/twitter/callback`
  },

  google: {
    clientID: '340645407309-bflkoa6tc9d45b3d1pj6ccb2pjirq3mg.apps.googleusercontent.com',
    clientSecret: 'Dr2LUgjDQYxssHdAocNeOGSg',
    callbackURL: `${host}/auth/google/callback`
  },

  vkontakte: {
    clientID: '5440905',
    clientSecret: 'hbbdt5pjFYkrVnVl5zXc',
    callbackURL: `${host}/auth/vkontakte/callback`
  }
};

const emailRelay = {
  host: 'smtp.mailgun.org',
  port: '587',
  auth: {
    user: 'postmaster@sandbox001cb5fd24b2453a801e4ace8bc5f6e3.mailgun.org',
    pass: env.mailgun.pass,
    apiKey: env.mailgun.apiKey,
    domain: 'sandbox001cb5fd24b2453a801e4ace8bc5f6e3.mailgun.org'
  }
};

// Development specific configuration
// ==================================
export default {
  sn,
  emailRelay,
  seedDB: false
};

'use strict';

export default {
  // List of user roles
  userRoles: ['guest', 'user', 'admin'],
  requestCount: 5,
  itemsPerPage: 10,
  defaultAvatar: '../../assets/images/default/avatar.png',
  defaultCommunity: '../../assets/images/default/community.png',
  defaultLogo: '../../assets/images/default/atsign.png'
};

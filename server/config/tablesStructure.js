const entity = {
  user: {
    name: 'Accounts',
    fields: [
      'name',
      'avatarurl',
      'email',
      'role',
      'active'
    ]
  },
  community: {
    name: 'Communities',
    fields: [
      'title',
      'parentId',
      'gameId',
      'tag',
      'entityId',
      'removed',
      'creatorId',
      'active'
    ]
  },
  game: {
    name: 'Games',
    fields: [
      'title',
      'logourl'
    ]
  },
  gameProfile: {
    name: 'GameProfiles',
    fields: [
      'name',
      'karma',
      'accountId',
      'entityId',
      'removed',
      'gameId',
      'active'
    ]
  }
};

export default entity;

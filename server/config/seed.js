/**
 * Populate DB with sample data on server start
 * to disable, edit config/environment/index.js, and set `seedDB: false`
 */
import fs from 'fs';
import sequelize from './databaseConnection.js';
import {} from '../../database/models/history.js';
import {} from '../../database/models/karma-coeffs.js';
import { associateManyToMany, associateOneToOne, associateOneToMany } from '../api/relations/relations.controller';
const PATH_TO_KARMA_TRIGGERS = 'server/config/triggers';

export default () => {
  associateManyToMany();
  associateOneToOne();
  associateOneToMany();

  sequelize.sync()
    .then(() => {
      return new Promise((res, rej) => {
        fs.readdir(PATH_TO_KARMA_TRIGGERS, (err, files) => {
          if (err) rej(err);
          res(files);
        });
      });
    })
    .then(files => {
      const triggers = [];
      files.forEach(file => {
        triggers.push(new Promise((res) => {
          fs.readFile(`${PATH_TO_KARMA_TRIGGERS}/${file}`, (err, query) => {
            res(sequelize.query(query.toString()));
          });
        }));
      });
      return Promise.all(triggers);
    })
    .then(() => console.log('------finished populating database-------'))
    .catch(console.log.bind(console));
};

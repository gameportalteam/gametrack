'use strict';

export default {
  mode: process.env.NODE_ENV || 'development',
  ip: process.env.OPENSHIFT_NODEJS_IP || process.env.IP || undefined,
  port: process.env.OPENSHIFT_NODEJS_PORT || process.env.PORT || 9000,
  mailgun: {
    pass: process.env.MAILGUN_PASS || '',
    apiKey: process.env.MAILGUN_API_KEY || ''
  }
};

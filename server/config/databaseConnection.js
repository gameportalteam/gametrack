import Sequelize from 'sequelize';
const baseConfig = require('./db');

export default new Sequelize(baseConfig.database, baseConfig.username, baseConfig.password, {
  host: process.env.POSTGRES_HOST || 'localhost',
  port: 5432,
  dialect: 'postgres',
  pool: {
    max: 5,
    min: 0,
    idle: 2000
  },
  omitNull: true
});

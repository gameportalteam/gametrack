import _ from 'lodash';
import path from 'path';
import env from './nodeEnv';
import entity from './tablesStructure';

const environmentConfig = require(`./environment/${env.mode}.js`).default;
const baseEnvironmentConfig = require('./environment').default;
const sharedEnvironmentConfig = require('./environment/shared.js').default;
const rootPath = path.normalize(path.join(__dirname, '/../..'));

const baseConfig = {
  env,
  root: rootPath,
  appPath: path.join(rootPath, 'client'),
  tmpPath: path.join(rootPath, '.tmp'),
  clientConfig: {}
};

export default _.merge(
  baseConfig,
  baseEnvironmentConfig,
  environmentConfig,
  sharedEnvironmentConfig,
  { entity }
);

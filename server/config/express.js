/**
 * Express configuration
 */

'use strict';

import express from 'express';
import favicon from 'serve-favicon';
import compression from 'compression';
import bodyParser from 'body-parser';
import cookieParser from 'cookie-parser';
import path from 'path';
import config from './index';
import passport from 'passport';
import ejs from 'ejs';

export default function (app) {
  const env = app.get('env');
  app.set('views', `${config.root}/server/views`);
  app.engine('html', ejs.renderFile);
  app.set('view engine', 'html');
  app.use(compression());
  app.use(bodyParser.urlencoded({ extended: false }));
  app.use(bodyParser.json());
  app.use(cookieParser());
  app.use(passport.initialize());


  if (env === 'production') {
    app.use(favicon(path.join(config.root, 'client', 'favicon.ico')));
    app.use(express.static(config.appPath));
  }

  if (env === 'development') {
    const connectLivereload = require('connect-livereload'); // eslint-disable-line global-require
    app.use(connectLivereload());
  }

  if (env === 'development' || env === 'test') {
    app.use(express.static(config.tmpPath));
    app.use(express.static(config.appPath));
  }
}

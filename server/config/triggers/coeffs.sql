CREATE OR REPLACE FUNCTION manage() RETURNS TRIGGER AS $manager$
  DECLARE
    x integer DEFAULT 1;
    karmaId integer;
    curentDate timestamp with time zone;
    history "Histories"%ROWTYPE;
    isPositive boolean;
    op integer DEFAULT 1;
  BEGIN
  
    IF TG_OP = 'DELETE' THEN 
      x = x * (-1);
      NEW=OLD;
      IF TG_TABLE_NAME = 'Likes' THEN
        IF NEW."type" > 0 THEN op=op * (-1);
        END IF;
      END IF;
    ELSE
      IF TG_TABLE_NAME = 'Likes' THEN
        IF NEW."type" < 0 THEN op=op * (-1);
        END IF;
      END IF;
    END IF;
    
    history."action"=NEW."entityType";
    history."operation"=TG_OP;
    
    IF TG_TABLE_NAME='ReportGPs' OR TG_TABLE_NAME='ReportCommunities'
      THEN history."sourceId"=NEW."fromId";
           history."entityOwnerId"=NEW."toId";
           history."targetEntityId"=NEW."toId";
           history."targetEntityType"='gameProfile';
           history."sourceEntityType"='gameProfile';
           SELECT "isPositive" INTO isPositive 
           FROM "ReportLists" 
           WHERE "id"=NEW."reportId";

           IF isPositive=false THEN op=op * (-1);
           END IF;
           
           IF TG_TABLE_NAME='ReportCommunities' 
             THEN history."targetEntityType"='community';
                   SELECT "creatorId" INTO history."entityOwnerId"
                   FROM "Communities"
                   WHERE "id"=NEW."toId";
           END IF;
    ELSE
      history."sourceId"=NEW."whoId";
      history."sourceEntityType"='gameProfile';
      SELECT "type" INTO history."targetEntityType"
      FROM "Entities" WHERE "id"=NEW."targetEntityId";

      IF history."targetEntityType"='gameProfile' THEN
        SELECT "id" INTO history."targetEntityId"
        FROM "GameProfiles"
        WHERE "entityId"= NEW."targetEntityId";
        history."entityOwnerId"=history."targetEntityId";

      ELSEIF history."targetEntityType"='comment' THEN
        SELECT "id","whoId" INTO history."targetEntityId", history."entityOwnerId"
        FROM "Comments" WHERE "entityId"=NEW."targetEntityId";

      ELSEIF history."targetEntityType"='community' THEN
        SELECT "id","creatorId" INTO history."targetEntityId",history."entityOwnerId"
        FROM "Communities"
        WHERE "entityId"=NEW."targetEntityId";

      ELSEIF history."targetEntityType"='game' THEN
        SELECT "id" INTO history."targetEntityId"
        FROM "Games"
        WHERE "entityId"=NEW."targetEntityId";
        history."entityOwnerId"=0;

      ELSEIF history."targetEntityType"='account' THEN
        SELECT "id" INTO history."targetEntityId"
        FROM "Accounts"
        WHERE "entityId"=NEW."targetEntityId";
        history."targetEntityType"='account';
        history."entityOwnerId"=history."targetEntityId";
        history."sourceEntityType"='account';
      END IF;
    END IF;

    SELECT "karmaId" INTO karmaId FROM "GameProfiles" WHERE "id"=history."entityOwnerId";
    curentDate = clock_timestamp();
    SELECT nextval('"Histories_id_seq"'::regclass) INTO history."id";
    
    history."updatedAt"=curentDate;
    history."createdAt"=curentDate;
    IF TG_OP !='UPDATE' THEN
      PERFORM updateGlobalCount(x,TG_TABLE_NAME);
      PERFORM updateUserCount(x,TG_TABLE_NAME,history."sourceId");
    END IF;
    IF history."sourceEntityType"='gameProfile' THEN
      history."karmaPoints"=updateKarma(op,karmaId, TG_TABLE_NAME, history."sourceId");
    END IF;

    INSERT INTO "Histories" VALUES (history.*); 
    RETURN NULL;
  END;
$manager$ LANGUAGE plpgsql;

-- TRIGGERS SECTION 

-- AFTER INSERT

DROP TRIGGER IF EXISTS commentInserted ON "Comments";

CREATE TRIGGER commentInserted
  AFTER INSERT
  ON "Comments"
FOR EACH ROW
EXECUTE PROCEDURE manage();

DROP TRIGGER IF EXISTS likeInserted ON "Likes";

CREATE TRIGGER likeInserted
  AFTER INSERT OR UPDATE
  ON "Likes"
FOR EACH ROW
EXECUTE PROCEDURE manage();

DROP TRIGGER IF EXISTS reportInserted ON "ReportGPs";

CREATE TRIGGER reportInserted
  AFTER INSERT
  ON "ReportGPs"
FOR EACH ROW
EXECUTE PROCEDURE manage();

DROP TRIGGER IF EXISTS reportCInserted ON "ReportCommunities";

CREATE TRIGGER reportCInserted
  AFTER INSERT
  ON "ReportCommunities"
FOR EACH ROW
EXECUTE PROCEDURE manage();

-- AFTER DELETE

DROP TRIGGER IF EXISTS likeDeleted ON "Likes";

CREATE TRIGGER likeDeleted
  AFTER DELETE
  ON "Likes"
FOR EACH ROW
EXECUTE PROCEDURE manage();

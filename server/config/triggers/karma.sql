DROP FUNCTION IF EXISTS updateGlobalCount(integer, name);

CREATE OR REPLACE FUNCTION updateGlobalCount(x integer,table_name name) RETURNS void AS $$
  BEGIN
    IF table_name='Likes' THEN
      UPDATE "KarmaCoeffs" SET "likeCount"="likeCount"+x WHERE "id"=1;
    ELSEIF table_name='Comments' THEN
      UPDATE "KarmaCoeffs" SET "commentCount"="commentCount"+x WHERE "id"=1;
    ELSEIF table_name='ReportGPs' OR table_name = 'ReportCommunities' THEN
      UPDATE "KarmaCoeffs" SET "reportCount"="reportCount"+x WHERE "id"=1;
    END IF;
  END;
$$ LANGUAGE plpgsql;

DROP FUNCTION IF EXISTS updateUserCount(integer, name, integer);

CREATE OR REPLACE FUNCTION updateUserCount(x integer,table_name name,sourceId integer)
RETURNS void AS $$
  DECLARE
    karmaId integer;
  BEGIN
    SELECT "karmaId" INTO karmaId
    FROM "GameProfiles" WHERE "entityId"=sourceId;
    IF table_name='Likes' THEN
      UPDATE "Karmas" SET "likeCount"="likeCount"+x WHERE "id"=karmaId;
    ELSEIF table_name = 'Comments' THEN
      UPDATE "Karmas" SET "commentCount"="commentCount"+x WHERE "id"=karmaId;
    ELSEIF table_name='ReportGPs' OR table_name='ReportCommunities' THEN
      UPDATE "Karmas" SET "reportCount"="reportCount"+x WHERE "id"=karmaId;
    END IF;
  END;
$$ LANGUAGE plpgsql;

DROP FUNCTION IF EXISTS updateKarma(integer, integer, name, integer);

CREATE OR REPLACE FUNCTION updateKarma(op integer, karmaId integer,table_name name,sourceId integer)
RETURNS double precision AS $$
  DECLARE
    totalCount bigint;
    count bigint;
    sourcePoints  double precision;
    sourceCount bigint;
    karma double precision;
  BEGIN

    SELECT sum("likeCount"+"commentCount"+"reportCount") INTO totalCount
    FROM "KarmaCoeffs" WHERE "id"=1;

    IF table_name = 'Likes' THEN
      SELECT "likePoints","likeCount" INTO sourcePoints, sourceCount
      FROM "Karmas" AS "karma",
           "GameProfiles" AS "source"
      WHERE "source"."entityId"=sourceId
      AND "karma"."id"="source"."karmaId";

      SELECT "likeCount" INTO count
      FROM "KarmaCoeffs" WHERE "id"=1;
      karma=calcKarma(count,totalCount,sourcePoints,sourceCount)*op;
      UPDATE "Karmas" SET "likePoints"="likePoints"+karma WHERE "id"=karmaId;

    ELSEIF table_name = 'Comments' THEN
      SELECT "commentPoints","commentCount" INTO sourcePoints, sourceCount 
      FROM "Karmas" AS "karma",
           "GameProfiles" AS "source" 
      WHERE "source"."entityId"=sourceId
      AND "karma"."id"="source"."karmaId";

      SELECT "commentCount" INTO count
      FROM "KarmaCoeffs" WHERE "id"=1;
      karma=calcKarma(count,totalCount,sourcePoints,sourceCount)*op;
      UPDATE "Karmas" SET "commentPoints"="commentPoints"+karma WHERE "id"=karmaId;

    ELSEIF table_name = 'ReportGPs' OR table_name = 'ReportCommunities' THEN
      SELECT "reportPoints","reportCount" INTO sourcePoints, sourceCount
      FROM "Karmas" AS "karma",
           "GameProfiles" AS "source"
      WHERE "source"."id"=sourceId
      AND "karma"."id"="source"."karmaId";

      SELECT "reportCount" INTO count
      FROM "KarmaCoeffs" WHERE "id"=1;
      karma=(@ calcKarma(count,totalCount,sourcePoints,sourceCount))*op;
      UPDATE "Karmas" SET "reportPoints"="reportPoints"+karma WHERE "id"=karmaId;
    END IF;
    RETURN karma;
  END;
$$ LANGUAGE plpgsql;

DROP FUNCTION IF EXISTS calcKarma(bigint, bigint, double precision, bigint);

CREATE OR REPLACE FUNCTION calcKarma(count bigint, totalCount bigint, sourcePoints double precision, sourceCount bigint)
  RETURNS double precision AS $$
  DECLARE
    diffCoeff integer;
    defaultPoints real;
  BEGIN
    SELECT "diffCoeff","defaultPoints" INTO diffCoeff, defaultPoints
    FROM "KarmaCoeffs" WHERE "id"=1;
    RETURN ((1-count/(totalCount+defaultPoints))*sourcePoints+log(sourceCount+defaultPoints))/diffCoeff+defaultPoints;
  END;
$$ LANGUAGE plpgsql;

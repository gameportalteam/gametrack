import winston from 'winston';
import path from 'path';

winston.emitErrs = true;

function formatter(options) {
  return `[${new Date().toISOString()}][${options.level.toUpperCase()}] -- '${(options.message || '')}`;
}

/**
 * Custom logger
 *
 * @method logger
 * @param {Object} faire module
 * @return {Object} logger
 * @public
 */
const logger = new winston.Logger({
  transports: [
    new winston.transports.File({
      name: 'error-file',
      level: 'error',
      filename: path.join(__dirname, '../../logs/error/errlogs.log'),
      handleExceptions: true,
      json: true,
      maxsize: 5242880,
      maxFiles: 5,
      colorize: false,
      formatter
    }),
    new winston.transports.File({
      name: 'info-file',
      level: 'info',
      filename: path.join(__dirname, '../../logs/info/logs.log'),
      json: true,
      maxsize: 5242880,
      maxFiles: 5,
      colorize: false,
      formatter
    }),
    new winston.transports.Console({
      level: 'debug',
      handleExceptions: true,
      json: false,
      colorize: true,
      formatter
    })
  ],
  exitOnError: false
});

export default logger;

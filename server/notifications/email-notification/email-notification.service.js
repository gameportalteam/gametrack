import * as nodemailer from 'nodemailer';
import config from '../../config';

const transporter = nodemailer.createTransport(config.emailRelay);

export function send(data) {
  if (!data.from) {
    data.from = 'support@gametrack.ru';
  }
  return transporter.sendMail(data);
}

'use strict';

import * as emailService from './email-notification/email-notification.service';

/**
 * Sends email to recepient
 *
 * @param {Object} data - e-mail message data
 * @see https://github.com/nodemailer/nodemailer#e-mail-message-fields
 */
export function notifyByEmail(data) {
  return emailService.send(data);
}

import likeService from './like.service';

/**
 * Set like essence
 */
export function set(req, res, next) {
  const currentUser = req.user;
  const like = req.body;

  if (!currentUser) {
    return res.status(401).end();
  }

  return likeService.setAsync(currentUser, like)
    .then(result => res.json(result))
    .catch(next);
}

/**
 * Reset like essence
 */
export function reset(req, res, next) {
  const currentUser = req.user;
  const like = req.body;

  if (!currentUser) {
    return res.status(401).end();
  }

  return likeService.resetAsync(currentUser, like)
    .then(result => {
      if (!result) {
        return res.status(404).end();
      }
      return res.json({ likesCount: result });
    })
    .catch(next);
}

'use strict';

import { Router } from 'express';
import * as controller from './like.controller';
import * as auth from '../../auth/auth.service';

const router = new Router();

router.post('/set', auth.addUserIfAuthenticated, controller.set);
router.post('/reset', auth.addUserIfAuthenticated, controller.reset);

export default router;

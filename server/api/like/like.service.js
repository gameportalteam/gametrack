'use strict';

import BaseService from '../BaseService';
import LikeModel from '../../../database/models/like';
import accountService from '../account/account.service';

class LikeService extends BaseService {
  getLike(targetEntityId, whoId) {
    return this.getOneAsync({
      where: {
        targetEntityId,
        whoId
      }
    });
  }

  countLikes(targetEntityId) {
    const countUp = this.countAsync({
      where: {
        targetEntityId,
        type: 1
      }
    });
    const countDown = this.countAsync({
      where: {
        targetEntityId,
        type: -1
      }
    });
    return Promise.all([countUp, countDown])
      .then(([resCountUp, resCountDown]) => {
        return {
          countUp: resCountUp,
          countDown: resCountDown
        };
      });
  }

  getProfileLikeAsync(user, gameId, entityId) {
    if (!user) {
      return Promise.resolve(null);
    }

    return accountService.getPrimaryProfileAsync(user.id, gameId)
      .then(primaryProfile => {
        if (!primaryProfile) {
          return Promise.resolve(null);
        }
        return this.getLike(entityId, primaryProfile.entityId);
      });
  }

  getUserLikeAsync(user, entityId) {
    if (!user) {
      return Promise.resolve(null);
    }

    return this.getLike(entityId, user.entityId);
  }

  setLikeAsync(owner, newLike) {
    newLike.whoId = owner.entityId;

    const opts = {
      where: {
        whoId: newLike.whoId,
        targetEntityId: newLike.targetEntityId
      }
    };

    return this.getOneAsync(opts)
      .then(like => {
        if (!like) {
          return this.createAsync(newLike);
        }
        return this.updateOneAsync(opts, newLike);
      })
      .then(like => {
        const count = this.countLikes(newLike.targetEntityId);
        return Promise.all([like, count]);
      })
      .then(([like, count]) => {
        return {
          userLike: like.toDto(),
          likesCount: count
        };
      });
  }

  setAsync(user, like) {
    return accountService.getOwner(user, like.gameId)
      .then(owner => {
        if (owner.reason) {
          return Promise.resolve(owner);
        }
        return this.setLikeAsync(owner, like);
      });
  }

  resetLikeAsync(owner, newLike) {
    newLike.whoId = owner.entityId;

    const opts = {
      where: {
        whoId: newLike.whoId,
        targetEntityId: newLike.targetEntityId,
        type: newLike.type
      }
    };

    return this.deleteOneAsync(opts)
      .then((like) => {
        if (!Array.isArray(like)) return Promise.resolve(null);
        return this.countLikes(newLike.targetEntityId);
      });
  }

  resetAsync(user, like) {
    return accountService.getOwner(user, like.gameId)
      .then(owner => {
        if (owner.reason) {
          return Promise.resolve(owner);
        }
        return this.resetLikeAsync(owner, like);
      });
  }
}

export default new LikeService(LikeModel);

'use strict';

import Account from '../../../database/models/account';
import Community from '../../../database/models/community';
import Comment from '../../../database/models/comment';
import Like from '../../../database/models/like';
import Entity from '../../../database/models/entity';
import GameProfile from '../../../database/models/game-profile';
import SNProfile from '../../../database/models/snprofile';
import Game from '../../../database/models/game';
import ReportGP from '../../../database/models/report-gp';
import Karma from '../../../database/models/karma';
import ReportCommunity from '../../../database/models/report-community';
import ReportList from '../../../database/models/report-list';
import GameProfileCommunity from '../../../database/models/gp-community';

export function associateManyToMany() {
  GameProfile.belongsToMany(Community, {
    as: 'communities',
    through: GameProfileCommunity,
    foreignKey: 'gameProfileId'
  });
  Community.belongsToMany(GameProfile, {
    as: 'gameProfiles',
    through: GameProfileCommunity,
    foreignKey: 'communityId'
  });
}

export function associateOneToOne() {
  Community.belongsTo(GameProfile, {
    as: 'creator',
    foreignKey: 'creatorId',
    constraints: false
  });
  Like.belongsTo(Entity, {
    as: 'who',
    foreignKey: 'whoId'
  });
  Account.belongsTo(Entity, {
    as: 'entity',
    foreignKey: 'entityId',
    constraints: false
  });
  Game.belongsTo(Entity, {
    as: 'entity',
    foreignKey: 'entityId',
    constraints: false
  });
  GameProfile.belongsTo(Entity, {
    as: 'entity',
    foreignKey: 'entityId',
    constraints: false
  });
  Comment.belongsTo(Entity, {
    as: 'entity',
    foreignKey: 'entityId',
    constraints: false
  });
  Community.belongsTo(Entity, {
    as: 'entity',
    foreignKey: 'entityId',
    constraints: false
  });
  ReportGP.belongsTo(ReportList, {
    as: 'report',
    foreignKey: 'reportId'
  });
  ReportGP.belongsTo(GameProfile, {
    as: 'from',
    foreignKey: 'fromId'
  });
  ReportGP.belongsTo(GameProfile, {
    as: 'to',
    foreignKey: 'toId'
  });
  ReportCommunity.belongsTo(ReportList, {
    as: 'report',
    foreignKey: 'reportId'
  });
  ReportCommunity.belongsTo(GameProfile, {
    as: 'from',
    foreignKey: 'fromId'
  });
  ReportCommunity.belongsTo(Community, {
    as: 'to',
    foreignKey: 'toId'
  });
  GameProfile.belongsTo(Game, {
    as: 'game',
    foreignKey: 'gameId'
  });
  GameProfile.belongsTo(Karma, {
    as: 'karma',
    foreignKey: 'karmaId'
  });
}

export function associateOneToMany() {
  Community.belongsTo(Game, {
    as: 'game',
    foreignKey: 'gameId'
  });
  Account.hasMany(SNProfile, {
    as: 'snProfiles',
    foreignKey: 'accountId'
  });
  SNProfile.belongsTo(Account, {
    as: 'account',
    foreignKey: 'accountId'
  });
  Entity.hasMany(Comment, {
    as: 'comments',
    foreignKey: 'targetEntityId'
  });
  Entity.hasMany(Like, {
    as: 'likes',
    foreignKey: 'targetEntityId'
  });
  Comment.belongsTo(Entity, {
    as: 'author',
    foreignKey: 'whoId'
  });
}


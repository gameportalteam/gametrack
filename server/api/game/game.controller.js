'use strict';

import config from '../../config';
import gameService from './game.service';

/**
 * Get list of games
 *
 * @example
 *  GET /api/games/
 *  GET /api/games?page=N
 *  GET /api/games?game=Title
 *  GET /api/games?page=N&game=Title
 */
export function index(req, res, next) {
  const obj = req.query.game ? { title: { $iLike: `%${req.query.game}%` } } : {};
  const itemsPerPage = config.itemsPerPage;
  const page = req.query.page || 1;

  return gameService.getAllAsync({
    where: obj,
    offset: itemsPerPage * (page - 1),
    limit: itemsPerPage,
    order: 'title ASC'
  }).then(games => res.status(200).json(games.map(x => x.toDto())))
    .catch(next);
}

/**
 * Get a single game
 *
 * @example
 *  GET /api/games/:id
 */
export function show(req, res, next) {
  const gameId = req.params.id;
  const user = req.user;

  return gameService.showGameAsync(gameId, user)
    .then(game => {
      if (!game) res.status(404).end();
      res.json(game);
    })
    .catch(next);
}

/**
 * Get list of game's comments
 *
 * @example
 *  GET /api/games/:id/comments
 */
export function getComments(req, res, next) {
  const gameId = req.params.id;
  const user = req.user;

  return gameService.showCommentsAsync(gameId, user)
    .then(comments => res.json(comments))
    .catch(next);
}

/**
 * Get list of game's likes
 *
 * @example
 *  GET /api/games/:id/likes
 */
export function getLikes(req, res, next) {
  return gameService.getLikesAsync(req.params.id)
    .then(likes => res.status(200).json(likes.map(x => x.toDto())))
    .catch(next);
}

/**
 * Get list of game's profiles
 *
 * @example
 *  GET /api/games/:id/profiles
 *  GET /api/games/:id/profiles?page=N
 *  GET /api/games/:id/profiles?name=Name
 *  GET /api/games/:id/profiles?page=N&name=Name
 */
export function getProfiles(req, res, next) {
  const itemsPerPage = config.itemsPerPage;
  const page = req.query.page || 1;
  const gameId = req.params.id;
  const query = req.query.name;

  return gameService.showProfilesAsync(query, page, itemsPerPage, gameId)
    .then(profiles => res.status(200).json(profiles))
    .catch(next);
}

/**
 * Get list of top N profiles in game
 *
 * @example
 *  GET /api/games/:id/topProfiles?count=N
 */
export function getTopProfiles(req, res, next) {
  return gameService.getTopProfilesAsync(req.params.id, req.query.count)
    .then(topProfiles => res.json(topProfiles.map(x => {
      const karma = x.dataValues.karma;
      const dto = x.toDto();
      dto.karma = karma.toFixed(4);
      return dto;
    })))
    .catch(next);
}

/**
 * Get list of game's communities
 *
 * @example
 *  GET /api/games/:id/communities
 */
export function getCommunities(req, res, next) {
  return gameService.getCommunitiesAsync(req.params.id)
    .then(communities => res.status(200).json(communities.map(x => x.toDto())))
    .catch(next);
}

import sequelize from '../../config/databaseConnection.js';
import BaseService from '../BaseService';
import Game from '../../../database/models/game';
import GameProfile from '../../../database/models/game-profile';
import CommunityService from '../community/community.service';
import LikeService from '../like/like.service';
import GameProfileService from '../game-profile/game-profile.service';
import entityService from '../entity/entity.service';
import Comment from '../../../database/models/comment';
import Karma from '../../../database/models/karma';
import _ from 'lodash';


class GameService extends BaseService {
  createAsync(game) {
    return entityService.createAsync()
      .then((entity) => {
        game.entityId = entity.id;
      })
      .then(() => Game.create(game))
      .then(gameObj => {
        entityService.updateByIdAsync(gameObj.entityId, {
          type: gameObj.entityType
        });
        return gameObj;
      });
  }

  getLikesAsync(id) {
    const opts = {
      where: {
        rootId: id,
        rootType: 'game'
      }
    };
    return LikeService.getAllAsync(opts);
  }

  getProfilesAsync(opts) {
    return GameProfileService.getAllAsync(opts)
      .then(profiles => {
        const dto = profiles.map(x => x.toDto());
        const sortedProfiles = dto.sort((a, b) => b.karma - a.karma);
        return Promise.resolve(sortedProfiles);
      });
  }

  getCommunitiesAsync(id) {
    const opts = {
      where: { gameId: id }
    };
    return CommunityService.getAllAsync(opts);
  }

  getTopProfilesAsync(id, count) {
    const query =
      `SELECT *,"k"."id", ("likePoints"+"commentPoints"+"reportPoints") as "karma"
       FROM "Karmas" as "k",
            "GameProfiles" as "gp"
      WHERE "removed" = FALSE
      AND "gameId" = :id
      AND "gp"."karmaId"="k"."id"
      ORDER BY "karma" DESC
      LIMIT :count;`;
    return sequelize.query(query, {
      replacements: { id, count },
      model: GameProfile
    });
  }

  getCommentsAsync(id) {
    const query = `
    SELECT
        "Comments".*,
        "GameProfiles"."nickname" as "authorName",
        "GameProfiles"."id" as "authorId",
        "GameProfiles"."gameId" as "gameId"
    FROM "Comments"
    INNER JOIN "GameProfiles" ON "Comments"."whoId" = "GameProfiles"."entityId"
    WHERE "Comments"."rootId" = :id
    ORDER BY "Comments"."path" ASC`;
    return sequelize.query(query, {
      replacements: { id },
      model: Comment
    });
  }

  showCommentsAsync(id, user) {
    return this.getCommentsAsync(id)
      .then(comments => comments.map(x => {
        const dto = x.toDto();
        const count = LikeService.countLikes(dto.entityId);
        const profileLike = LikeService.getProfileLikeAsync(user, dto.gameId, dto.entityId);
        return Promise.all([Promise.resolve(dto), count, profileLike]);
      }))
      .then(comments => Promise.all(comments))
      .then(comments => {
        const dto = comments.map(([comment, likesCount, profileLike]) => {
          comment.likesCount = likesCount;
          comment.userLike = profileLike;
          return comment;
        });
        return dto;
      });
  }

  showGameAsync(id, user) {
    return this.getByIdAsync(id)
      .then(game => {
        if (!game) {
          return Promise.reject(null);
        }
        const dto = game.toDto();
        const count = LikeService.countLikes(dto.entityId);
        const profileLike = LikeService.getProfileLikeAsync(user, dto.gameId, dto.entityId);
        return Promise.all([Promise.resolve(dto), count, profileLike]);
      })
      .then(([dto, count, profileLike]) => {
        dto.likesCount = count;
        dto.userLike = profileLike && profileLike.toDto();
        return Promise.resolve(dto);
      }, null, game => game);
  }

  showProfilesAsync(query, page, itemsPerPage, gameId) {
    const name = query ? {
      where: {
        nickname: {
          $iLike: `%${query}%`
        }
      },
      order: 'nickname ASC'
    } : {};
    const otherOpts = {
      where: {
        gameId
      },
      include: [{
        model: Karma, as: 'karma'
      }],
      offset: itemsPerPage * (page - 1),
      limit: itemsPerPage
    };
    const opts = _.merge(otherOpts, name);

    return this.getProfilesAsync(opts);
  }
}

export default new GameService(Game);

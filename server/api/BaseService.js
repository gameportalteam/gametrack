'use strict';

import db from '../config/databaseConnection.js';

class BaseService {
  constructor(model) {
    this.model = model;
  }

  createAsync(obj) {
    const model = this.model;
    return model.create(obj);
  }

  getAllAsync(opts) {
    const model = this.model;
    return model.findAll(opts);
  }

  getOneAsync(obj) {
    const model = this.model;
    return model.findOne(obj);
  }

  getByIdAsync(id) {
    const model = this.model;
    return model.findById(id);
  }

  countAsync(obj) {
    const model = this.model;
    return model.count(obj);
  }

  updateOneAsync(obj, newItem) {
    return this.getOneAsync(obj)
      .then((item) => {
        if (item) {
          return item.updateAttributes(newItem);
        }
        return item;
      });
  }

  updateByIdAsync(id, newItem) {
    return this.getByIdAsync(id)
      .then((item) => {
        if (item) {
          return item.updateAttributes(newItem);
        }
        return item;
      });
  }

  /**
   * @param {Object} instance - db model instance
   * @param {Object} updates - key/value pairs to update on instance
   * @param {Object} options
   *
   * @return {Promise} - updated instance
   *
   * @see http://docs.sequelizejs.com/en/latest/api/instance/#updateupdates-options-promisethis
   */
  updateInstanceAsync(instance, updates, options) {
    return instance.update(updates, options);
  }

  deleteOneAsync(obj) {
    return this.getOneAsync(obj)
      .then((item) => {
        if (item) {
          return item.destroy();
        }
        return item;
      });
  }

  deleteByIdAsync(id) {
    return this.getByIdAsync(id)
      .then((item) => {
        if (item) {
          return item.destroy();
        }
        return item;
      });
  }

  query(query) {
    return db.query(query);
  }
}

export default BaseService;

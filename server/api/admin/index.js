'use strict';

import { Router } from 'express';
import * as controller from './admin.controller';
import * as auth from '../../auth/auth.service';

const router = new Router();

router.get('/logs', auth.addUserIfAuthenticated, controller.getLogs);

export default router;

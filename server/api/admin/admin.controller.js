
export function getLogs(req, res) {
  const user = req.user;

  if (user && user.role === 'admin') {
    const file = './logs/error/errlogs.log';
    return res.download(file);
  }

  return res.status(404).end();
}

import request from 'request';
import config from '../../config';
import jwt from 'jsonwebtoken';
import accountService from './account.service';
import gameProfileService from '../game-profile/game-profile.service';

/**
 * Get list of users
 *
 * @example
 *  GET /api/users/
 *  GET /api/users?page=N
 *  GET /api/users?name=Name
 *  GET /api/users?page=N&name=Name
 */
export function index(req, res, next) {
  const obj = req.query.name ? { name: { $iLike: `%${req.query.name}%` } } : {};
  const itemsPerPage = config.itemsPerPage;
  const page = req.query.page || 1;

  return accountService.getAllAsync({
    where: obj,
    offset: itemsPerPage * (page - 1),
    limit: itemsPerPage,
    order: 'name ASC'
  }).then(users => res.status(200).json(users.map(x => x.toDto())))
    .catch(next);
}

/**
 * Get list of users
 *
 * @example
 *  GET /api/users/
 *  GET /api/users?count=N
 *  GET /api/users?name=Name
 *  GET /api/users?count=N&name=Name
 */
export function getUsersForAdmin(req, res, next) {
  const user = req.user;

  if (user && user.role === 'admin') {
    return accountService.getAllAsync()
      .then(users => res.status(200).json(users.map(x => x.toDto())))
      .catch(next);
  }

  return res.status(404).end();
}

/**
 * Creates a new user
 *
 * @example
 *  POST /api/users
 */
export function create(req, res, next) {
  accountService.createAsync(req.body)
    .then(user => {
      const token = jwt.sign({ id: user.id }, config.secrets.session, {
        expiresIn: 60 * 60 * 5
      });
      res.json({ token });
    })
    .catch(next);
}

/**
 * Get a single user
 *
 * @example
 *  GET /api/users/:id
 */
export function show(req, res, next) {
  const userId = req.params.id;
  const currentUser = req.user;

  return accountService.showUserAsync(userId, currentUser)
    .then(user => {
      if (!user) {
        return res.status(404).end();
      }
      return res.json(user);
    })
    .catch(next);
}

/**
 * Get game profiles by user id
 *
 * @example
 *  GET /api/users/:id/gameProfiles
 */
export function getGameProfiles(req, res, next) {
  const currentUser = req.user;
  const userId = parseInt(req.params.id, 10);

  if (currentUser && parseInt(currentUser.id, 10) === userId) {
    return accountService.getGameProfilesAsync(userId)
      .then(profiles => {
        res.json(profiles.map(x => x.toDto()));
      })
      .catch(next);
  }

  return res.status(200).end();
}

/**
 * Get primary profile by user and game id
 *
 * @example
 *  GET /:id/primary-profile/:gameId
 */
export function getPrimaryProfile(req, res, next) {
  const accountId = req.user ? req.user.id : -1;
  const gameId = req.params.gameId;

  return accountService.getPrimaryProfileAsync(accountId, gameId)
    .then(profile => res.json(profile))
    .catch(next);
}

/**
 * Connect game profile to user account
 *
 * @example
 *  POST /api/users/:id/connectProfile/:profileId
 */
export function connectGameProfile(req, res, next) {
  const accountId = req.params.id;
  const gameProfileId = req.params.profileId;

  gameProfileService.getByIdAsync(gameProfileId)
    .then(profile => {
      if (!profile) {
        res.status(404).end();
      }
      return gameProfileService.updateInstanceAsync(profile, {
        accountId: accountId
      });
    })
    .then(profile => res.json(profile.toDto()))
    .catch(next);
}

/**
 * Deletes a user
 * restriction: 'admin'
 *
 * @example
 *  DELETE /api/users/:id
 */
export function destroy(req, res, next) {
  accountService.deleteByIdAsync(req.params.id)
    .then(() => {
      res.status(204).end();
    })
    .catch(next);
}

/**
 * Change a users password
 *
 * @example
 *  PUT /api/users/:id/password
 */
export function changePassword(req, res, next) {
  const userId = req.user.id;
  const oldPass = String(req.body.oldPassword);
  const newPass = String(req.body.newPassword);

  accountService.getByIdAsync(userId)
    .then(user => {
      if (user.authenticate(oldPass)) {
        user.password = newPass;
        return user.saveAsync()
          .then(() => {
            res.status(204).end();
          })
          .catch(next);
      }
      return res.status(403).end();
    });
}

/**
 * Get my info
 *
 * @example
 *  GET /api/users/me
 */
export function me(req, res, next) {
  const userId = req.user.id;
  accountService.getByIdAsync(userId)
    .then(user => { // don't ever give out the password or salt
      if (!user) {
        return res.status(401).end();
      }
      return res.json(user.toDto());
    })
    .catch(next);
}

/**
 * Authentication callback
 */
export function authCallback(req, res) {
  res.redirect('/');
}

/** Get list of account's comments
 *
 * @example
 *  GET /api/accounts/:id/comments
 */
export function getComments(req, res, next) {
  const userId = req.params.id;
  const user = req.user;

  return accountService.showCommentsAsync(userId, user)
    .then(comments => res.json(comments))
    .catch(next);
}

/**
 * Change a users avatar url
 *
 * @example
 *  PUT /api/users/avatarurl
 */
export function saveAvatarUrl(req, res, next) {
  const userId = req.user.id;
  const newAvatarUrl = String(req.body.avatarUrl);

  request.get({
    method: 'HEAD',
    url: newAvatarUrl,
  }, (err, response) => {
    if (err) {
      res.status(406).json({ message: 'INVALID_AVATAR_URL_ERROR' });
      return;
    }

    if (response.headers['content-type'].indexOf('image') === -1) {
      res.status(406).json({ message: 'NOT_IMAGE_URL_ERROR' });
      return;
    }

    if (+response.headers['content-length'] > 1 * 1024 * 1024) { // 1mb
      res.status(406).json({ message: 'BIG_IMAGE_ERROR' });
      return;
    }

    saveUserProperty(userId, {
      name: 'avatarurl',
      value: newAvatarUrl,
    }).then(() => res.status(200).end())
      .catch(next);
  });
}

export function saveName(req, res, next) {
  const userId = req.user.id;
  const newName = String(req.body.name);

  saveUserProperty(userId, {
    name: 'name',
    value: newName,
  }).then(() => res.status(200).end())
    .catch(next);
}

function saveUserProperty(userId, property) {
  return accountService.getByIdAsync(userId)
    .then(user => accountService.updateInstanceAsync(user, {
      [property.name]: property.value,
    }));
}

'use strict';

import { Router } from 'express';
import * as controller from './account.controller';
import * as auth from '../../auth/auth.service';

const router = new Router();

router.get('/', controller.index);
router.get('/admin', auth.addUserIfAuthenticated, controller.getUsersForAdmin);
router.delete('/:id', auth.hasRole('admin'), controller.destroy);
router.get('/me', auth.isAuthenticated(), controller.me);
router.put('/:id/password', auth.isAuthenticated(), controller.changePassword);
router.get('/:id', auth.addUserIfAuthenticated, controller.show);
router.post('/', controller.create);
router.get('/:id/gameProfiles', auth.addUserIfAuthenticated, controller.getGameProfiles);
router.get('/primary-profile/:gameId', auth.addUserIfAuthenticated, controller.getPrimaryProfile);
router.post('/:id/connectProfile/:profileId', auth.addUserIfAuthenticated, controller.connectGameProfile);
router.get('/:id/comments', auth.addUserIfAuthenticated, controller.getComments);
router.put('/saveAvatarUrl', auth.isAuthenticated(), controller.saveAvatarUrl);
router.put('/saveName', auth.isAuthenticated(), controller.saveName);

export default router;

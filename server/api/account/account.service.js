'use strict';

import BaseService from '../BaseService';
import UserModel from '../../../database/models/account';
import SNProfile from '../../../database/models/snprofile';
import Game from '../../../database/models/game';
import gameProfileService from '../game-profile/game-profile.service';
import entityService from '../entity/entity.service';
import likeService from '../like/like.service';
import sequelize from '../../config/databaseConnection.js';
import Comment from '../../../database/models/comment';

class AccountService extends BaseService {
  createAsync(account) {
    const name = account.email.match(/^[^\s@]+/);
    account.name = name && name.shift();

    return entityService.createAsync()
      .then(entity => {
        account.entityId = entity.id;
        return UserModel.create(account)
          .then((res) => entityService.updateByIdAsync(entity.id, {
            type: res.entityType
          }).then(() => res));
      });
  }

  getByIdAsync(id) {
    return this.getOneAsync({
      where: { id },
      include: [{ model: SNProfile, as: 'snProfiles' }]
    });
  }

  getGameProfilesAsync(id) {
    return gameProfileService.getAllAsync({
      where: { accountId: id },
      include: [{ model: Game, as: 'game' }]
    });
  }

  getPrimaryProfileAsync(accountId, gameId) {
    return gameProfileService.getOneAsync({
      where: {
        accountId,
        gameId,
        isPrimary: true
      }
    })
      .then(profile => {
        return profile ? profile.toDto() : profile;
      });
  }

  // This functionality will be changed
  // createAsync(obj) {
  //   const newUser = new this.model(obj);
  //   newUser.provider = 'local';
  //   newUser.role = 'user';
  //   return newUser.saveAsync();
  // }

  getCommentsAsync(id) {
    const query = `
    SELECT
        "Comments".*,
        "Accounts"."name" as "authorName",
        "Accounts"."id" as "authorId"
    FROM "Comments"
    INNER JOIN "Accounts" ON "Comments"."whoId" = "Accounts"."entityId"
    WHERE "Comments"."rootId" = :id
    ORDER BY "Comments"."path" ASC`;
    return sequelize.query(query, {
      replacements: { id },
      model: Comment
    });
  }

  showCommentsAsync(id, user) {
    return this.getCommentsAsync(id)
      .then(comments => comments.map(x => {
        const dto = x.toDto();
        const count = likeService.countLikes(dto.entityId);
        const userLike = likeService.getUserLikeAsync(user, dto.entityId);
        return Promise.all([Promise.resolve(dto), count, userLike]);
      }))
      .then(comments => Promise.all(comments))
      .then(comments => {
        const dto = comments.map(([comment, likesCount, userLike]) => {
          comment.likesCount = likesCount;
          comment.userLike = userLike;
          return comment;
        });
        return dto;
      });
  }

  showUserAsync(id, currentUser) {
    return this.getOneAsync({
      where: { id: id }
    })
      .then(user => {
        if (!user) {
          return Promise.reject(null);
        }
        const dto = user.toDto();
        const count = likeService.countLikes(dto.entityId);
        const userLike = likeService.getUserLikeAsync(currentUser, dto.entityId);
        return Promise.all([Promise.resolve(dto), count, userLike]);
      })
      .then(([dto, count, userLike]) => {
        dto.likesCount = count;
        dto.userLike = userLike && userLike.toDto();
        return dto;
      }, null, user => user);
  }

  getOwner(user, gameId) {
    if (!gameId) return Promise.resolve(user);
    return this.getPrimaryProfileAsync(user.id, gameId)
      .then(primaryProfile => {
        if (!primaryProfile) {
          return Promise.resolve({
            reason: 'gp not exist'
          });
        }
        return Promise.resolve(primaryProfile);
      });
  }
}

export default new AccountService(UserModel);

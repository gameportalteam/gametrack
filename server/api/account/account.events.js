/**
 * User model events
 */
import { EventEmitter } from 'events';
import User from '../../../database/models/account';
import _ from 'lodash';
const UserEvents = new EventEmitter();

// Set max event listeners (0 == unlimited)
UserEvents.setMaxListeners(0);

// Model events
const events = {
  save: 'save',
  remove: 'remove'
};

function emitEvent(event) {
  return (doc) => {
    UserEvents.emit(`${event}:${doc.id}`, doc);
    UserEvents.emit(event, doc);
  };
}

// Register the event emitter to the model events
_.forEach(events, (event, e) => {
  User.schema.post(e, emitEvent(event));
});

export default UserEvents;

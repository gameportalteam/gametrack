'use strict';

import BaseService from '../BaseService';
import Karma from '../../../database/models/karma';

class KarmaService extends BaseService {
}

export default new KarmaService(Karma);

'use strict';

import communityService from './community.service';
import config from '../../config';

/**
 * Get list of communities
 *
 * @example
 *  GET /api/communities?page=N
 */
export function index(req, res, next) {
  const itemsPerPage = config.itemsPerPage;
  const page = req.query.page || 1;

  const opts = {
    offset: itemsPerPage * (page - 1),
    limit: itemsPerPage,
    order: 'title ASC'
  };

  return communityService.getAllAsync(opts)
    .then(communities => res.status(200).json(communities.map(x => x.toDto())))
    .catch(next);
}

/**
 * Get a single community
 *
 * @example
 *  GET /api/communities/:id
 */
export function show(req, res, next) {
  const user = req.user;
  communityService.showCommunityAsync(req.params.id, user)
    .then(community => {
      if (!community) {
        res.status(404).end();
      }
      res.status(200).json(community);
    })
    .catch(next);
}

/**
 * Get list of community's communities
 *
 * @example
 *  GET /api/communities/:id/communities
 */
export function getCommunities(req, res, next) {
  return communityService.getCommunitiesAsync(req.params.id)
    .then(communities => res.status(200).json(communities.map(x => x.toDto())))
    .catch(next);
}

/**
 * Get list of community's comments
 *
 * @example
 *  GET /api/communities/:id/comments
 */
export function getComments(req, res, next) {
  const user = req.user;
  return communityService.showCommentsAsync(req.params.id, user)
    .then(comments => res.json(comments))
    .catch(next);
}

/**
 * Get list of community's likes
 *
 * @example
 *  GET /api/communities/:id/likes
 */
export function getLikes(req, res, next) {
  return communityService.getLikesAsync(req.params.id)
    .then(likes => res.json(likes.map(x => x.toDto())))
    .catch(next);
}

/**
 * Get list of community's profiles
 *
 * @example
 *  GET /api/communities/:id/profiles
 */
export function getProfiles(req, res, next) {
  return communityService.getProfilesAsync(req.params.id)
    .then(community => res.status(200).json(community.gameProfiles.map(x => x.toDto())))
    .catch(next);
}

/**
 * Get list of top N profiles in community
 *
 * @example
 *  GET /api/communities/:id/topProfiles?count=N
 */
export function getTopProfiles(req, res, next) {
  return communityService.getTopProfilesAsync(req.params.id, req.query.count)
    .then(profiles => res.json(profiles.map(x => {
      const karma = x.dataValues.karma;
      const dto = x.toDto();
      dto.karma = karma.toFixed(4);
      return dto;
    })))
    .catch(next);
}

export function create(req, res, next) {
  const newCommunity = req.body.newCommunity;
  communityService.createAsync(newCommunity)
    .then(community => res.status(200).json(community.toDto()))
    .catch(next);
}

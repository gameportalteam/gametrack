import sequelize from '../../config/databaseConnection.js';
import BaseService from '../BaseService';
import Community from '../../../database/models/community';
import GameProfile from '../../../database/models/game-profile';
import Game from '../../../database/models/game';
import likeService from '../like/like.service';
import entityService from '../entity/entity.service';
import Comment from '../../../database/models/comment';

class CommunityService extends BaseService {
  createAsync(community) {
    return entityService.createAsync()
      .then((entity) => {
        community.entityId = entity.id;
        return Community.create(community)
          .then((res) => entityService.updateByIdAsync(entity.id, {
            type: res.entityType
          }).then(() => res));
      });
  }

  getCommunityAsync(id) {
    const opts = {
      where: { id: id, removed: false },
      include: [
        { model: GameProfile, as: 'creator' },
        { model: Game, as: 'game' }
      ]
    };
    return this.getOneAsync(opts);
  }

  getCommunitiesAsync(id) {
    const opts = {
      where: { parentId: id }
    };
    return this.getAllAsync(opts);
  }

  getLikesAsync(id) {
    const opts = {
      where: {
        rootId: id,
        rootType: 'community'
      }
    };
    return likeService.getAllAsync(opts);
  }

  getProfilesAsync(id) {
    const opts = {
      where: { id: id },
      include: [{
        model: GameProfile, as: 'gameProfiles'
      }]
    };
    return this.getOneAsync(opts);
  }

  getTopProfilesAsync(id, count) {
    const query =
      `SELECT *, "k"."id", ("likePoints"+"commentPoints"+"reportPoints") as "karma"
     FROM "Karmas" as "k",
          "GameProfiles" as "gp"
     WHERE "removed" = FALSE
     AND "gp"."id" IN
     (SELECT "gameProfileId" FROM "GameProfileCommunities"
      WHERE "communityId" = :id)
     AND "gp"."karmaId"="k"."id"
     ORDER BY "karma" DESC
     LIMIT :count;`;
    return sequelize.query(query, {
      replacements: { id, count },
      model: GameProfile
    });
  }

  showCommunityAsync(id, user) {
    return this.getCommunityAsync(id)
      .then((community) => {
        if (!community) return Promise.resolve(null);
        const dto = community.toDto();
        const count = likeService.countLikes(dto.entityId);
        const userLike = likeService.getProfileLikeAsync(user, community.gameId, dto.entityId);
        return Promise.all([Promise.resolve(dto), count, userLike]);
      })
      .then(([dto, count, userLike]) => {
        dto.likesCount = count;
        dto.userLike = userLike && userLike.toDto();
        return dto;
      });
  }

  getCommentsAsync(id) {
    const query = `
    SELECT
        "Comments".*,
        "GameProfiles"."nickname" as "authorName",
        "GameProfiles"."id" as "authorId",
        "GameProfiles"."gameId" as "gameId"
    FROM "Comments"
    INNER JOIN "GameProfiles" ON "Comments"."whoId" = "GameProfiles"."entityId"
    WHERE "Comments"."rootId" = :id
    ORDER BY "Comments"."path" ASC`;
    return sequelize.query(query, {
      replacements: { id },
      model: Comment
    });
  }

  showCommentsAsync(id, user) {
    return this.getCommentsAsync(id)
      .then(comments => comments.map(x => {
        const dto = x.toDto();
        const count = likeService.countLikes(dto.entityId);
        const profileLike = likeService.getProfileLikeAsync(user, dto.gameId, dto.entityId);
        return Promise.all([Promise.resolve(dto), count, profileLike]);
      }))
      .then(comments => Promise.all(comments))
      .then(comments => {
        const dto = comments.map(([comment, likesCount, profileLike]) => {
          comment.likesCount = likesCount;
          comment.userLike = profileLike;
          return comment;
        });
        return dto;
      });
  }
}

export default new CommunityService(Community);

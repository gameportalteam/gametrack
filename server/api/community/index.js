'use strict';

import { Router } from 'express';
import * as controller from './community.controller';
import * as auth from '../../auth/auth.service';

const router = new Router();

router.post('/', auth.isAuthenticated(), controller.create);
router.get('/', controller.index);
router.get('/:id', auth.addUserIfAuthenticated, controller.show);
router.get('/:id/communities', controller.getCommunities);
router.get('/:id/comments', auth.addUserIfAuthenticated, controller.getComments);
router.get('/:id/likes', controller.getLikes);
router.get('/:id/profiles', controller.getProfiles);
router.get('/:id/topProfiles', controller.getTopProfiles);
router.get('/:id/comments', auth.addUserIfAuthenticated, controller.getComments);


export default router;

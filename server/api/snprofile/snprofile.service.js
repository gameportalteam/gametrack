'use strict';

import BaseService from '../BaseService';
import SNProfileModel from '../../../database/models/snprofile';

class SNProfileService extends BaseService {
  /**
   * @param {String} snId - social network profile id
   * @param {String} type - provider (ex. 'facebook')
   */
  getBySNIdAsync(snId, type) {
    return this.getOneAsync({
      where: {
        snId: snId,
        snType: type
      },
      include: [{ all: true }]
    });
  }
}

export default new SNProfileService(SNProfileModel);

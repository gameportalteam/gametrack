'use strict';

import commentService from './comment.service';

/**
 * Creates a new comment
 *
 * @example
 *  POST /api/comments
 */
export function create(req, res, next) {
  const newComment = req.body.newComment;
  const user = req.user;

  commentService.createCommentAsync(user, newComment)
    .then(comment => res.status(200).json(comment))
    .catch(next);
}

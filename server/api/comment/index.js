'use strict';

import { Router } from 'express';
import * as controller from './comment.controller';
import * as auth from '../../auth/auth.service';

const router = new Router();

router.post('/', auth.isAuthenticated(), controller.create);

export default router;

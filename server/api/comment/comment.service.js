'use strict';

import BaseService from '../BaseService';
import Comment from '../../../database/models/comment';
import entityService from '../entity/entity.service';
import accountService from '../account/account.service';

class CommentService extends BaseService {
  createAsync(owner, newComment) {
    newComment.whoId = owner.entityId;

    return entityService.createAsync()
      .then((entity) => {
        newComment.entityId = entity.id;
        newComment.path += `/${entity.id}`;
        return Comment.create(newComment);
      })
      .then(comment => {
        entityService.updateByIdAsync(comment.entityId, {
          type: comment.entityType
        });
        return comment.toDto();
      });
  }

  createCommentAsync(user, comment) {
    return accountService.getOwner(user, comment.gameId)
      .then(owner => {
        if (owner.reason) return Promise.resolve(owner);
        return this.createAsync(owner, comment);
      });
  }
}

export default new CommentService(Comment);

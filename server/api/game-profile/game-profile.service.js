'use strict';

import BaseService from '../BaseService';
import GameProfile from '../../../database/models/game-profile';
import entityService from '../entity/entity.service';
import karmaService from '../karma/karma.service';
import Karma from '../../../database/models/karma';
import likeService from '../like/like.service';
import sequelize from '../../config/databaseConnection.js';
import Comment from '../../../database/models/comment';


class GameProfileService extends BaseService {
  createAsync(profile) {
    const opts = {
      where: {
        accountId: profile.accountId,
        gameId: profile.gameId,
        isPrimary: true
      }
    };

    const entityPromise = entityService.createAsync();
    const karmaPromise = karmaService.createAsync();
    const primaryProfile = this.getOneAsync(opts);
    return Promise.all([entityPromise, karmaPromise, primaryProfile])
      .then(([entity, karma, primary]) => {
        if (!primary && profile.accountId) {
          profile.isPrimary = true;
        }
        profile.entityId = entity.id;
        profile.karmaId = karma.id;
        return GameProfile.create(profile)
          .then((res) => entityService.updateByIdAsync(entity.id, {
            type: res.entityType
          }).then(() => res));
      });
  }

  setAsPrimary(newPrimaryId, accountId, gameId) {
    const oldPrimary = {
      where: {
        accountId: accountId,
        isPrimary: true,
        gameId: gameId
      }
    };
    const newPrimary = { where: { id: newPrimaryId } };
    return this.updateOneAsync(oldPrimary, { isPrimary: false })
      .then(() => {
        return this.updateOneAsync(newPrimary, { isPrimary: true });
      });
  }

  getCommentsAsync(id) {
    const query = `
    SELECT
        "Comments".*,
        "GameProfiles"."nickname" as "authorName",
        "GameProfiles"."id" as "authorId",
        "GameProfiles"."gameId" as "gameId"
    FROM "Comments"
    INNER JOIN "GameProfiles" ON "Comments"."whoId" = "GameProfiles"."entityId"
    WHERE "Comments"."rootId" = :id
    ORDER BY "Comments"."path" ASC`;
    return sequelize.query(query, {
      replacements: { id },
      model: Comment
    });
  }

  showGameProfileAsync(id, user) {
    return this.getOneAsync({
      where: { id: id },
      include: [{ model: Karma, as: 'karma' }]
    })
      .then(profile => {
        if (!profile || profile.removed) {
          return Promise.reject(null);
        }
        const dto = profile.toDto();
        const count = likeService.countLikes(dto.entityId);
        const userLike = likeService.getProfileLikeAsync(user, profile.gameId, dto.entityId);
        return Promise.all([Promise.resolve(dto), count, userLike]);
      })
      .then(([dto, count, userLike]) => {
        dto.likesCount = count;
        dto.userLike = userLike && userLike.toDto();
        return dto;
      }, null, gp => gp);
  }

  showCommentsAsync(id, user) {
    return this.getCommentsAsync(id)
      .then(comments => comments.map(x => {
        const dto = x.toDto();
        const count = likeService.countLikes(dto.entityId);
        const userLike = likeService.getProfileLikeAsync(user, dto.gameId, dto.entityId);
        return Promise.all([Promise.resolve(dto), count, userLike]);
      }))
      .then(comments => Promise.all(comments))
      .then(comments => {
        const dto = comments.map(([comment, likesCount, userLike]) => {
          comment.likesCount = likesCount;
          comment.userLike = userLike;
          return comment;
        });
        return dto;
      });
  }
}

export default new GameProfileService(GameProfile);

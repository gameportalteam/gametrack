'use strict';

import { Router } from 'express';
import * as controller from './game-profile.controller';
import * as auth from '../../auth/auth.service';

const router = new Router();

router.get('/', controller.index);
router.get('/free', controller.getFreeGameProfiles);
router.get('/:id', auth.addUserIfAuthenticated, controller.show);
router.post('/', controller.create);
router.post('/setAsPrimary', controller.setAsPrimary);
router.post('/:id/requestOwnership', auth.isAuthenticated(), controller.requestOwnership);
router.get('/:id/comments', auth.addUserIfAuthenticated, controller.getComments);

export default router;

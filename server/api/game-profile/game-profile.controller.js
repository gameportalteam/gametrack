'use strict';

import config from '../../config';
import gameProfileService from './game-profile.service';
import * as notificationService from '../../notifications/notification.service';
import _ from 'lodash';

/**
 * Get list of game profiles
 *
 * @example
 *  GET /api/gameProfiles?page=N
 *
 */
export function index(req, res, next) {
  const itemsPerPage = config.itemsPerPage;
  const page = req.query.page || 1;

  const opts = {
    offset: itemsPerPage * (page - 1),
    limit: itemsPerPage,
    order: 'nickname ASC'
  };

  gameProfileService.getAllAsync(opts)
    .then(profiles => res.status(200).json(profiles.map(x => x.toDto())))
    .catch(next);
}

/**
 * Get list of free (accountId not specified) game profiles
 *
 * @example
 *  GET /api/gameProfiles/free
 */
export function getFreeGameProfiles(req, res, next) {
  const obj = req.query.name ? { where: { nickname: { $iLike: `%${req.query.name}%` } } } : {};
  const opts = _.merge(obj,
    {
      where: { accountId: 0 },
      limit: req.query.count
    });
  gameProfileService.getAllAsync(opts)
    .then(profiles => res.status(200).json(profiles.map(x => x.toDto())))
    .catch(next);
}

/**
 * Creates a new game profile
 *
 * @example
 *  POST /api/gameProfiles
 */
export function create(req, res, next) {
  const newProfile = {
    nickname: req.body.name,
    accountId: req.body.accountId,
    gameId: req.body.gameId
  };

  return gameProfileService.createAsync(newProfile)
    .then(profile => res.status(200).json(profile.toDto()))
    .catch(next);
}

/**
 * Get a single game profile
 *
 * @example
 *  GET /api/gameProfiles/:id
 */
export function show(req, res, next) {
  const user = req.user;
  return gameProfileService.showGameProfileAsync(req.params.id, user)
    .then(gp => {
      if (!gp) {
        res.status(404).end();
      }
      res.json(gp);
    })
    .catch(next);
}

/**
 * Request game profile ownership conflict resolution
 *
 * @example
 *  POST /api/gameProfiles/:id/requestOwnership
 */
export function requestOwnership(req, res, next) {
  const user = req.user;
  const profileId = req.params.id;

  gameProfileService.getByIdAsync(profileId)
    .then(profile => {
      if (!profile) res.status(404).end();
      profile = profile.toDto();
      const notificationMessage = {
        to: 'support@gametrack.ru',
        subject: 'Game profile ownership conflict',
        text: `User ${user.name}(${user.id}) says that he is the owner of game profile ${profile.name}(${profileId}).
           Please, resolve conflict.`
      };
      return notificationService.notifyByEmail(notificationMessage);
    })
    .then(() => res.status(200).end())
    .catch(next);
}

/**
 * Set new primary account
 *
 * @example
 *  POST /api/gameProfiles/setAsPrimary
 */
export function setAsPrimary(req, res, next) {
  const newPrimaryId = req.body.newPrimaryId;
  const accountId = req.body.accountId;
  const gameId = req.body.gameId;
  gameProfileService.setAsPrimary(newPrimaryId, accountId, gameId)
    .then(primary => res.json(primary.toDto()))
    .catch(next);
}

/** Get list of gameprofile's comments
 *
 * @example
 *  GET /api/gameProfiles/:id/comments
 */
export function getComments(req, res, next) {
  const user = req.user;
  return gameProfileService.showCommentsAsync(req.params.id, user)
    .then(comments => res.json(comments))
    .catch(next);
}

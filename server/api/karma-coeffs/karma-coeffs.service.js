'use strict';

import BaseService from '../BaseService';
import KarmaCoeffs from '../../../database/models/karma-coeffs';

class KarmaCoeffsService extends BaseService {
  setDefaultPoints(defaultPoints) {
    return this.updateByIdAsync(1, { defaultPoints: defaultPoints });
  }

  setDiffCoeff(diffCoeff) {
    return this.updateByIdAsync(1, { diffCoeff: diffCoeff });
  }
}

export default new KarmaCoeffsService(KarmaCoeffs);

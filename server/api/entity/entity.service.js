import BaseService from '../BaseService';
import Entity from '../../../database/models/entity';

class EntityService extends BaseService {
}

export default new EntityService(Entity);

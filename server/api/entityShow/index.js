'use strict';

import { Router } from 'express';
import * as controller from './entityShow.controller';

const router = new Router();

router.get('/', controller.show);

export default router;

'use strict';

import { default as config } from '../../config/environment/shared';
import entity from '../../config/tablesStructure';
import communityService from '../../../database/models/community';
import accountService from '../account/account.service';

const services = {
  community: communityService,
  user: accountService
};

/**
 * Get list of entities
 *
 * @example
 *  GET /api/communities
 */
export function show(req, res, next) {
  const itemsPerPage = config.itemsPerPage;
  const query = req.query;
  const attributes = [].concat(query.attributes);

  const entityType = query.entityType;
  const opts = {
    attributes: attributes,
    where: JSON.parse(query.where),
    order: query.order,
    offset: itemsPerPage * (query.page - 1),
    limit: itemsPerPage
  };

  // need to be sure that the params is safe
  const wrongAttrs = opts.attributes
    .filter(e => entity[entityType].fields.indexOf(e) === -1)
    .length;
  const wrongWhere = Object.keys(opts.where)
    .filter(e => entity[entityType].fields.indexOf(e) === -1)
    .length;

  if (wrongAttrs || wrongWhere) {
    res.status(500).json('error');
  }

  services[entityType].getAllAsync(opts).
    then(entities => {
      res.status(200).json(entities);
    })
    .catch(next);
}

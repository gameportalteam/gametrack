import BaseService from '../../BaseService';
import ReportCommunityModel from '../../../../database/models/report-community';
import Community from '../../../../database/models/community';

class ReportCommunityService extends BaseService {
  getAllAsync(obj) {
    obj.include.push({ model: Community, as: 'to' });
    return super.getAllAsync(obj);
  }
}

export default new ReportCommunityService(ReportCommunityModel);

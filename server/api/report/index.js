import { Router } from 'express';
import * as controller from './report.controller';
import * as auth from '../../auth/auth.service';

const router = new Router();

router.post('/', auth.isAuthenticated(), controller.create);
router.get('/:type/:id', controller.reports);

export default router;

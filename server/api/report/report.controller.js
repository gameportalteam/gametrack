'use strict';

import GameProfile from '../../../database/models/game-profile';
import ReportList from '../../../database/models/report-list';
import gpService from './report-gp/report-gp.service';
import communityService from './report-community/report-community.service';
const services = {
  gameProfile: gpService,
  community: communityService
};

/**
 * Creates a new report
 *
 * @example
 *  POST /api/reports
 */
export function create(req, res, next) {
  return services[req.body.type].createAsync(req.body.report)
    .then(report => {
      const to = report.getTo();
      const from = report.getFrom();
      const rep = report.getReport();
      return Promise.all([Promise.resolve(report), to, from, rep]);
    })
    .then(([report, to, from, rep]) => {
      report.to = to;
      report.from = from;
      report.report = rep;
      res.status(200).json(report.toDto());
    })
    .catch(next);
}

/**
 * Get list of reports by type and id
 *
 * @example
 *  GET /api/reports/:type/:id
 */
export function reports(req, res, next) {
  return services[req.params.type].getAllAsync({
    where: { toId: req.params.id },
    include: [
      { model: GameProfile, as: 'from' },
      { model: ReportList, as: 'report' }
    ]
  })
    .then(rs => res.status(200).json(rs.map(x => x.toDto())))
    .catch(next);
}

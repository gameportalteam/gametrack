import BaseService from '../../BaseService';
import ReportGP from '../../../../database/models/report-gp';
import GameProfile from '../../../../database/models/game-profile';

class ReportGPService extends BaseService {
  getAllAsync(obj) {
    obj.include.push({ model: GameProfile, as: 'to' });
    return super.getAllAsync(obj);
  }
}

export default new ReportGPService(ReportGP);

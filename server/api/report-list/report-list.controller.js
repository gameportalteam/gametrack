import reportListService from './report-list.service.js';

/**
 * Get list of reports
 *
 * @example
 *  GET /api/reportslist
 */
export function index(req, res, next) {
  return reportListService.getAllAsync()
    .then(reports => {
      const complains = [];
      const commends = [];
      reports.forEach(report => {
        if (report.isPositive) commends.push(report.toDto());
        else complains.push(report.toDto());
      });
      res.json({
        COMPLAIN: complains,
        COMMEND: commends
      });
    })
    .catch(next);
}

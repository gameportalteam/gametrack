import { Router } from 'express';
import * as controller from './report-list.controller';
import * as auth from '../../auth/auth.service';

const router = new Router();

router.get('/', auth.isAuthenticated(), controller.index);

export default router;

'use strict';

import BaseService from '../BaseService';
import ReportList from '../../../database/models/report-list';

class ReportListService extends BaseService {
}

export default new ReportListService(ReportList);

import express from 'express';
import config from './config';
import http from 'http';
import log from './log/logger.js';
import fs from 'fs';
import path from 'path';

import routesConfig from './routes.js';
import expressConfig from './config/express.js';

// Populate databases with sample data
import seed from './config/seed.js';
seed();

// Setup server
const app = express();
const logsPath = path.join(__dirname, '../logs');
if (!fs.existsSync(logsPath)) {
  fs.mkdirSync(logsPath);
}
const infoLogsPath = path.join(__dirname, '../logs/info');
const errorLogsPath = path.join(__dirname, '../logs/error');

if (!fs.existsSync(infoLogsPath)) {
  fs.mkdirSync(infoLogsPath);
}
if (!fs.existsSync(errorLogsPath)) {
  fs.mkdirSync(errorLogsPath);
}
log.info('application is starting...');
const server = http.createServer(app);

expressConfig(app);
routesConfig(app);

// unhandled errors logging
// eslint-disable-line no-unused-vars
app.use((err, req, res, next) => {
  log.error(err.message + err.stack);
  next(err);
});

// Start server
setImmediate(() => {
  app.angularFullstack = server.listen(config.env.port, config.env.ip, () => {
    console.log('Express server listening on %d, in %s mode', config.env.port, config.env.mode);
  });
});

// Expose app
exports = module.exports = app;

'use strict';

import { Router } from 'express';
import config from '../config';
import User from '../api/account/account.service';
// Passport Configuration
require('./local/passport').setup(User, config.sn);
require('./facebook/passport').setup(User, config.sn);
require('./google/passport').setup(User, config.sn);
require('./twitter/passport').setup(User, config.sn);
require('./vkontakte/passport').setup(User, config.sn);

const router = new Router();

router.use('/local', require('./local').default);
router.use('/facebook', require('./facebook').default);
router.use('/twitter', require('./twitter').default);
router.use('/google', require('./google').default);
router.use('/vkontakte', require('./vkontakte').default);

export default router;

'use strict';

import express from 'express';
import passport from 'passport';
import jwt from 'jsonwebtoken';
import { signToken, isAuthenticated } from '../auth.service';
import accountService from '../../api/account/account.service';
import config from '../../config';

const router = new express.Router();

router.post('/', (req, res, next) => {
  passport.authenticate('local', (err, user, info) => {
    const error = err || info;
    if (error && error.message === 'This email is not registered.') {
      return accountService.createAsync(req.body)
        .then(u => {
          const token = jwt.sign({ id: u.id }, config.secrets.session, {
            expiresIn: 60 * 60 * 5
          });
          res.json({ token });
        });
    }
    if (error) {
      return res.status(401).json(error);
    }
    if (!user) {
      return res.status(404).json({ message: 'Something went wrong, please try again.' });
    }

    const token = signToken(user.id, user.role);
    res.json({ token });
  })(req, res, next);
});

router.post('/connect', isAuthenticated(), (req, res, next) => {
  const user = req.user;
  accountService.updateInstanceAsync(user,
    {
      email: req.body.email,
      password: req.body.password
    })
    .then(u => res.json(u.toDto()))
    .catch(next);
});

export default router;

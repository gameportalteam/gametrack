'use strict';

import config from '../config';
import jwt from 'jsonwebtoken';
import expressJwt from 'express-jwt';
import compose from 'composable-middleware';
import accountService from '../api/account/account.service';
import snProfileService from '../api/snprofile/snprofile.service';

const validateJwt = expressJwt({
  secret: config.secrets.session
});

/**
 * Attaches the user object to the request if authenticated
 * Otherwise returns 403
 */
export function isAuthenticated() {
  return compose()
  // Validate jwt
    .use((req, res, next) => {
      // allow access_token to be passed through query parameter as well
      if (req.query && req.query.hasOwnProperty('access_token')) {
        req.headers.authorization = `Bearer ${req.query.access_token}`;
      }
      validateJwt(req, res, next);
    })
    // Attach user to request
    .use((req, res, next) => {
      accountService.getByIdAsync(req.user.id)
        .then(user => {
          if (!user) {
            return res.status(401).end();
          }
          req.user = user;
          next();
        })
        .catch(next);
    });
}

/**
 * Attaches the user object to the request if authenticated
 * Otherwise goes to the next handler
 */
export function addUserIfAuthenticated(req, res, next) {
  jwt.verify(req.cookies.token, config.secrets.session, (err, decoded) => {
    if (err) {
      next();
    } else {
      accountService.getByIdAsync(decoded.id)
        .then(user => {
          if (!user) {
            return res.status(401).end();
          }
          req.user = user;
          next();
        })
        .catch(next);
    }
  });
}

/**
 * Checks if the user role meets the minimum requirements of the route
 */
export function hasRole(roleRequired) {
  if (!roleRequired) {
    throw new Error('Required role needs to be set');
  }

  return compose()
    .use(isAuthenticated())
    .use((req, res, next) => {
      if (config.userRoles.indexOf(req.user.role) >=
        config.userRoles.indexOf(roleRequired)) {
        next();
      } else {
        res.status(403).send('Forbidden');
      }
    });
}

/**
 * Returns a jwt token signed by the app secret
 */
export function signToken(id, role) {
  return jwt.sign({ id: id, role: role }, config.secrets.session, {
    expiresIn: 60 * 60 * 5
  });
}

/**
 * Set token cookie directly for oAuth strategies
 */
export function setTokenCookie(req, res) {
  if (!req.user) {
    return res.status(404).send('It looks like you aren\'t logged in, please try again.');
  }
  const token = signToken(req.user.id, req.user.role);
  res.cookie('token', token);
  res.redirect('/');
}

/**
 * Disconnect social network profile(remove snId), but keep user info
 *
 * @param {String} provider - SN name (ex. 'facebook')
 */
export function disconnectOAuth(provider) {
  return (req, res, next) => {
    const user = req.user;
    user.getSnProfiles({
      where: { snType: provider }
    })
      .then(profiles => {
        const profile = profiles[0];
        return snProfileService.updateInstanceAsync(profile, { snId: null });
      })
      .then(() => accountService.getByIdAsync(user.id))
      .then(updatedUser => res.json(updatedUser.toDto()))
      .catch(next);
  };
}

'use strict';

import express from 'express';
import passport from 'passport';
import * as auth from '../auth.service';

const router = new express.Router();

router
  .get('/', passport.authenticate('facebook', {
    scope: ['email', 'user_about_me'],
    failureRedirect: '/signup',
    session: false
  }))

  .get('/callback', auth.addUserIfAuthenticated, passport.authenticate('facebook', {
    failureRedirect: '/signup',
    session: false
  }), auth.setTokenCookie)

  .post('/disconnect', auth.isAuthenticated(), auth.disconnectOAuth('facebook'));

export default router;

import passport from 'passport';
import { Strategy as FacebookStrategy } from 'passport-facebook';
import { processOAuth, OAuthProfile } from '../passport.service';

export function setup(User, config) {
  passport.use(new FacebookStrategy({
    clientID: config.facebook.clientID,
    clientSecret: config.facebook.clientSecret,
    callbackURL: config.facebook.callbackURL,
    profileFields: [
      'displayName',
      'emails'
    ],
    passReqToCallback: true
  }, (req, accessToken, refreshToken, profile, done) => {
    const fbProfile = new OAuthProfile({
      name: profile.displayName,
      email: profile.emails[0].value,
      role: 'user',
      provider: 'facebook',
      providerInfo: profile._json
    });
    processOAuth(req, fbProfile, User, done);
  }));
}

import passport from 'passport';
import { Strategy as VKontakteStrategy } from 'passport-vkontakte';
import { processOAuth, OAuthProfile } from '../passport.service';

export function setup(User, config) {
  passport.use(new VKontakteStrategy({
    clientID: config.vkontakte.clientID,
    clientSecret: config.vkontakte.clientSecret,
    callbackURL: config.vkontakte.callbackURL,
    display: 'popup',
    passReqToCallback: true
  }, (req, accessToken, refreshToken, profile, done) => {
    const vkProfile = new OAuthProfile({
      name: profile.displayName,
      role: 'user',
      provider: 'vkontakte',
      providerInfo: profile._json
    });
    processOAuth(req, vkProfile, User, done);
  }));
}

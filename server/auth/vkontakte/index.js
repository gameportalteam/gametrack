import express from 'express';
import passport from 'passport';
import * as auth from '../auth.service';

const router = new express.Router();

router
  .get('/', passport.authenticate('vkontakte', {
    scope: ['email'],
    failureRedirect: '/signup',
    session: false
  }))

  .get('/callback', auth.addUserIfAuthenticated, passport.authenticate('vkontakte', {
    failureRedirect: '/signup',
    session: false
  }), auth.setTokenCookie)

  .post('/disconnect', auth.isAuthenticated(), auth.disconnectOAuth('vkontakte'));

export default router;

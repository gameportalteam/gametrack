'use strict';

import snProfileService from '../api/snprofile/snprofile.service';
import accountService from '../api/account/account.service';

/**
 * OAuth responce processor
 * If user is not logged in, authenticate him or create new account
 * If user is already logged in, add 3d-party profile info to existing account
 *
 * @param {Object} req - request
 * @param {OAuthProfile} profile - 3d-party profile info
 * @param {UserService} User - user service
 * @param {Function} done - passport.js done callback
 */
export function processOAuth(req, profile, User, done) {
  if (!req.user) {
    snProfileService.getBySNIdAsync(profile.data.id.toString(), profile.provider)
      .then(snProfile => {
        if (snProfile) {
          return done(null, snProfile.account);
        }
        const account = accountService.createAsync({
          name: profile.name,
          email: profile.email,
          role: profile.role
        });
        snProfile = snProfileService.createAsync({
          snId: profile.data.id,
          snType: profile.provider,
          data: profile.data
        });
        return Promise.all([account, snProfile]);
      })
      .then(([account, snProfile]) => account.setSnProfiles([snProfile]))
      .then(acc => done(null, acc))
      .catch(done);
  } else { // User already logged in, add 3d-party account info
    const profileData = {
      snId: profile.data.id,
      snType: profile.provider,
      data: profile.data
    };
    req.user.getSnProfiles({
      where: { snType: profile.provider }
    })
      .then(snProfiles => {
        const snProfile = snProfiles[0];
        if (snProfile) {
          return snProfileService.updateInstanceAsync(snProfile, profileData);
        }
        return snProfileService.createAsync(profileData)
          .then(res => req.user.addSnProfiles([res]));
      })
      .then(user => done(null, user))
      .catch(done);
  }
}

export class OAuthProfile {

  constructor(profile) {
    this.name = profile.name;
    this.username = profile.username;
    this.email = profile.email;
    this.role = profile.role;
    this.provider = profile.provider;
    this.data = profile.providerInfo;
  }
}

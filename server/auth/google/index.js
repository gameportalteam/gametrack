'use strict';

import express from 'express';
import passport from 'passport';
import { setTokenCookie, addUserIfAuthenticated } from '../auth.service';

const router = new express.Router();

router
  .get('/', passport.authenticate('google', {
    failureRedirect: '/signup',
    scope: [
      'profile',
      'email'
    ],
    session: false
  }))
  .get('/callback', addUserIfAuthenticated, passport.authenticate('google', {
    failureRedirect: '/signup',
    session: false
  }), setTokenCookie);

export default router;

import passport from 'passport';
import { OAuth2Strategy as GoogleStrategy } from 'passport-google-oauth';
import { processOAuth, OAuthProfile } from '../passport.service';

export function setup(User, config) {
  passport.use(new GoogleStrategy({
    clientID: config.google.clientID,
    clientSecret: config.google.clientSecret,
    callbackURL: config.google.callbackURL,
    passReqToCallback: true
  }, (req, accessToken, refreshToken, profile, done) => {
    const googleProfile = new OAuthProfile({
      name: profile.displayName,
      username: profile.emails[0].value.split('@')[0],
      email: profile.emails[0].value,
      role: 'user',
      provider: 'google',
      providerInfo: profile._json
    });
    processOAuth(req, googleProfile, User, done);
  }));
}

'use strict';

import express from 'express';
import passport from 'passport';
import { setTokenCookie, addUserIfAuthenticated } from '../auth.service';

const router = new express.Router();

router
  .get('/', passport.authenticate('twitter', {
    failureRedirect: '/signup',
    session: false
  }))
  .get('/callback', addUserIfAuthenticated, passport.authenticate('twitter', {
    failureRedirect: '/signup',
    session: false
  }), setTokenCookie);

export default router;

import passport from 'passport';
import { Strategy as TwitterStrategy } from 'passport-twitter';
import { processOAuth, OAuthProfile } from '../passport.service';

export function setup(User, config) {
  passport.use(new TwitterStrategy({
    consumerKey: config.twitter.clientID,
    consumerSecret: config.twitter.clientSecret,
    callbackURL: config.twitter.callbackURL
  }, (req, token, tokenSecret, profile, done) => {
    profile._json.id = profile.id;
    const twitterProfile = new OAuthProfile({
      name: profile.displayName,
      username: profile.username,
      role: 'user',
      provider: 'twitter',
      providerInfo: profile._json
    });
    processOAuth(req, twitterProfile, User, done);
  }));
}

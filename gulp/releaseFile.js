import config from './config.js';
import gulp from 'gulp';
import fs from 'fs';
import { paths, clientPath } from './paths.js';


gulp.task('build:releaseFile', createReleaseFile);

function createReleaseFile(done) {
  if (config.env.mode !== 'production') return done();

  const releaseMessage = [
    `BUILD: ${process.env.BUILD_NUMBER}`,
    `VSC number: ${process.env.VCS_NUMBER}`,
    `DATE: ${new Date()}`
  ].join('\n');


  fs.writeFile(`${paths.dist}/${clientPath}/RELEASE.txt`, releaseMessage, error => {
    if (error) throw error;

    done();
  });

  return;
}

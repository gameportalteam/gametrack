export const clientPath = 'client';
export const serverPath = 'server';
export const databasePath = 'database';
export const paths = {
  client: {
    assets: `${clientPath}/assets/**/*`,
    images: `${clientPath}/assets/images/**/*`,
    scripts: [
      `${clientPath}/**/*.js`,
      `!${clientPath}/bower_components/**/*`,
      `!${clientPath}/theme/**/*`,
      `!${clientPath}/config/**`
    ],
    styles: [`${clientPath}/{app,theme,components}/**/*.scss`, `${clientPath}/{app,theme,components}/**/*.css`],
    mainStyle: `${clientPath}/app/app.scss`,
    views: `${clientPath}/{app,components}/**/*.html`,
    mainView: `${clientPath}/index.html`,
    test: ['tests/unit/client/**/*.{spec,mock}.js', 'tests/integration/client/**/*.{spec,mock}.js'],
    e2e: ['tests/e2e/**/*.spec.js'],
    bower: `${clientPath}/bower_components/`
  },
  server: {
    scripts: [`${serverPath}/**/*.js`],
    triggers: [`${serverPath}/config/triggers/*.sql`],
    json: [`${serverPath}/**/*.json`],
    test: {
      integration: ['tests/integration/server/**/*.integration.js', 'mocha.global.js'],
      unit: ['tests/unit/server/**/*.spec.js', 'mocha.global.js']
    }
  },
  database: {
    scripts: [`${databasePath}/**/*.js`, `${databasePath}/**/**/*.js`]
  },
  karma: 'karma.conf.js',
  dist: 'dist'
};

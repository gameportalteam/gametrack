const env = (process.env.NODE_ENV || 'development').toLowerCase();

export default {
  env: {
    mode: env,
    port: 9000
  },
  isSourcemaps: env === 'development'
};

FROM node:4-wheezy

MAINTAINER GameTrack

RUN mkdir -p /usr/src/app
WORKDIR /usr/src/app

# Copy built solution
COPY ./dist /usr/src/app
# Install production dependencies
RUN npm install --production


EXPOSE 9000

CMD [ "npm", "start" ]



set -e

gametrack_network="gametrack"

if ! docker network ls | grep -q ${gametrack_network}; then
	echo 'Create docker network:'
  	docker network create ${gametrack_network}
fi
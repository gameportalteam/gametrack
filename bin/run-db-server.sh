set -e

home_user_dir="$HOME"
postgres_db_dir="${home_user_dir}/database/postgres"
docker_image_name="gametrack-postgres"

mkdir -p "${postgres_db_dir}"

if ! docker ps -a | grep -q ${docker_image_name}; then
  docker run \
    --name ${docker_image_name} \
    -e "POSTGRES_USER=gametrack_user" \
    -e "POSTGRES_PASSWORD=gMtRcKaEa42" \
    -e "POSTGRES_DB=gametrack_db" \
    -e "PGDATA=/var/lib/postgresql/data/pgdata" \
    -v "${postgres_db_dir}:/var/lib/postgresql/data/pgdata" \
    --cap-add=ALL \
    --net=gametrack \
    -d postgres
fi
set -e

database_host_name="gametrack-postgres"
gametrack_network="gametrack"
docker_image_name="gametrack-node"
node_env="production"

# Delete node container
if docker ps | grep -q ${docker_image_name}; then
  echo 'Stop docker container:'
  docker stop ${docker_image_name}
fi

# Delete node images
if docker ps -a | grep -q ${docker_image_name}; then
  echo 'Removing docker container:'
  docker rm ${docker_image_name}
fi

(
  docker build -t "gametrack/gametrack-node-application" .
)

echo 'Run app server:'
docker run \
  --name gametrack-node \
  --net=gametrack \
  --env "POSTGRES_HOST=${database_host_name}" \
  --env "NODE_ENV=${node_env}" \
  -p 9000:9000 \
  -d gametrack/gametrack-node-application npm start
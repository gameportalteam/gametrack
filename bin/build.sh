#/bin/bash
#
# Build and start node server

# Exit script with error if it is occured
set -e

current_dir=$(pwd)
gametrack_dir="${current_dir}/gametrack"
gametrack_network="gametrack"

# Get application files
rm -rf "${gametrack_dir}"
git clone --depth=1 git@bitbucket.org:gameportalteam/gametrack.git "${gametrack_dir}"

(
  cd "${gametrack_dir}"
  docker build -t "gametrack/gametrack-node-application" .
)

if docker network ls | grep -q ${gametrack_network}; then
  docker network create ${gametrack_network}
fi

(
  /bin/bash "${gametrack_dir}/bin/deploy-mongo.sh"
  /bin/bash "${gametrack_dir}/bin/deploy-gametrack-node.sh"
)

'use strict';

class NavbarController {

  constructor(Auth, $translate, Languages, appConfig) {
    this.isLoggedIn = Auth.isLoggedIn;
    this.isAdmin = Auth.isAdmin;
    this.getCurrentUser = Auth.getCurrentUser();
    this.defaultAvatar = appConfig.defaultAvatar;

    Languages.get().$promise
      .then(res => {
        const lang = $translate.use();
        this.languages = res;
        this.currentLanguage = this.languages.filter(e => e.code === lang).pop();
      });

    this.switchLang = (key) => {
      $translate.use(key);
      this.currentLanguage = this.languages.filter(e => e.code === key).pop();
    };
    this.switchSelection = (item) => {
      this.header = item;
    };
  }
}

angular.module('GameTrack')
  .controller('NavbarController', NavbarController);

'use strict';

function AuthService($location, $http, $cookies, $q, appConfig, Util, User, GameProfiles) {
  const safeCb = Util.safeCb;
  let currentUser = {};
  let currentGP = {};
  const userRoles = appConfig.userRoles || [];

  if ($cookies.get('token') && $location.path() !== '/logout') {
    currentUser = User.getMe();
  }

  const Auth = {

    /**
     * Authenticate user and save token
     *
     * @param  {Object}   user     - login info
     * @param  {Function} callback - optional, function(error, user)
     * @return {Promise}
     */
    login({ email, password }, callback) {
      return $http.post('/auth/local', {
        email: email,
        password: password
      })
        .then(res => {
          $cookies.put('token', res.data.token);
          currentUser = User.getMe();
          return currentUser.$promise;
        })
        .then(user => {
          safeCb(callback)(null, user);
          return user;
        })
        .catch(err => {
          Auth.logout();
          safeCb(callback)(err.data);
          return $q.reject(err.data);
        });
    },

    disconnectOAuth(provider) {
      return $http.post(`/auth/${provider}/disconnect`)
        .then(res => res.data)
        .catch(console.log.bind(console));
    },

    connectLocalAccount(email, password) {
      return $http.post('/auth/local/connect', { email, password })
        .then(res => res.data)
        .catch(console.log.bind(console));
    },

    /**
     * Delete access token and user info
     */
    logout() {
      $cookies.remove('token');
      currentUser = {};
      currentGP = {};
    },

    /**
     * Create a new user
     *
     * @param  {Object}   user     - user info
     * @param  {Function} callback - optional, function(error, user)
     * @return {Promise}
     */
    createUser(user, callback) {
      return User.save(user,
        (data) => {
          $cookies.put('token', data.token);
          currentUser = User.getMe();
          return safeCb(callback)(null, user);
        },
        (err) => {
          Auth.logout();
          return safeCb(callback)(err);
        }).$promise;
    },

    /**
     * Change password
     *
     * @param  {String}   oldPassword
     * @param  {String}   newPassword
     * @param  {Function} callback    - optional, function(error, user)
     * @return {Promise}
     */
    changePassword(oldPassword, newPassword, callback) {
      return User.changePassword({ id: currentUser.id }, {
        oldPassword: oldPassword,
        newPassword: newPassword
      }, () => {
        return safeCb(callback)(null);
      }, (err) => {
        return safeCb(callback)(err);
      }).$promise;
    },

    /**
     * Gets all available info on a user
     *   (synchronous|asynchronous)
     *
     * @param  {Function|*} callback - optional, funciton(user)
     * @return {Object|Promise}
     */
    getCurrentUser(callback) {
      if (!callback) {
        return currentUser;
      }

      const value = (currentUser.hasOwnProperty('$promise')) ?
        currentUser.$promise : currentUser;
      return $q.when(value)
        .then(user => {
          safeCb(callback)(user);
          return user;
        }, () => {
          safeCb(callback)({});
          return {};
        });
    },

    /**
     * Check if a user is logged in
     *   (synchronous|asynchronous)
     *
     * @param  {Function|*} callback - optional, function(is)
     * @return {Bool|Promise}
     */
    isLoggedIn(callback) {
      if (!callback) {
        return currentUser.hasOwnProperty('role');
      }

      return Auth.getCurrentUser(null)
        .then(user => {
          const is = user.hasOwnProperty('role');
          safeCb(callback)(is);
          return is;
        });
    },

    /**
     * Check if a user has a specified role or higher
     *   (synchronous|asynchronous)
     *
     * @param  {String}     role     - the role to check against
     * @param  {Function|*} callback - optional, function(has)
     * @return {Bool|Promise}
     */
    hasRole(role, callback) {
      const hasRole = (r, h) => {
        return userRoles.indexOf(r) >= userRoles.indexOf(h);
      };

      if (!callback) {
        return hasRole(currentUser.role, role);
      }

      return Auth.getCurrentUser(null)
        .then(user => {
          const has = (user.hasOwnProperty('role')) ?
            hasRole(user.role, role) : false;
          safeCb(callback)(has);
          return has;
        });
    },

    /**
     * Check if a user is an admin
     *   (synchronous|asynchronous)
     *
     * @param  {Function|*} callback - optional, function(is)
     * @return {Bool|Promise}
     */
    isAdmin(...args) {
      return Auth.hasRole
        .apply(Auth, [].concat.apply(['admin'], args));
    },

    /**
     * Get auth token
     *
     * @return {String} - a token string used for authenticating
     */
    getToken() {
      return $cookies.get('token');
    },

    getCurrentProfile: (gameId) => {
      return User.getPrimaryProfile({ gameId: gameId }).$promise
        .then(profile => {
          currentGP = profile || {};
          return currentGP;
        });
    },

    createProfile: (name, gameId) => {
      return GameProfiles.create({
        name,
        gameId: gameId,
        accountId: currentUser.id
      }).$promise
        .then(profile => {
          return profile;
        });
    }

  };

  return Auth;
}

angular.module('GameTrack.auth')
  .factory('Auth', AuthService);

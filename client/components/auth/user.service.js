'use strict';

function UserResource($resource) {
  return $resource('/api/users/:id/:controller/:gameId', {
    id: '@_id'
  }, {
    addGameProfile: {
      method: 'POST',
      url: '/api/users/:id/:controller/:gameProfileId',
      params: {
        controller: 'connectProfile',
        id: '@id',
        gameProfileId: '@gameProfileId'
      }
    },
    changePassword: {
      method: 'PUT',
      params: {
        controller: 'password'
      }
    },
    getMe: {
      method: 'GET',
      params: {
        id: 'me'
      }
    },
    getById: {
      method: 'GET'
    },
    getAll: {
      method: 'GET',
      isArray: true
    },
    getGameProfiles: {
      method: 'GET',
      params: {
        controller: 'gameprofiles'
      },
      isArray: true
    },
    getPrimaryProfile: {
      method: 'GET',
      params: {
        controller: 'primary-profile',
        gameId: '@gameId'
      }
    }
  });
}

angular.module('GameTrack.auth')
  .factory('User', UserResource);


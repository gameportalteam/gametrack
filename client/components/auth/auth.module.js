'use strict';

angular.module('GameTrack.auth', [
  'GameTrack.constants',
  'GameTrack.util',
  'ngCookies',
  'ui.router'
])
  .config(($httpProvider) => {
    $httpProvider.interceptors.push('authInterceptor');
  });

'use strict';

angular.module('GameTrack')
  .controller('ConnectController', function connectController(Auth, $window) {
    this.connectOAuth = (provider) => {
      $window.location.href = `/auth/${provider}`;
    };

    this.disconnectOAuth = (provider) => {
      Auth.disconnectOAuth(provider)
        .then(user => {
          this.currentUser = user;
        });
    };

    this.connectLocalAccount = (form, email, user) => {
      if (form.$valid) {
        Auth.connectLocalAccount(email, user)
          .then(u => {
            this.currentUser = u;
          });
      } else {
        form.$setDirty();
      }
    };
  });

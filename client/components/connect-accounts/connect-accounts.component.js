'use strict';

angular.module('GameTrack')
  .component('connectAccounts', {
    templateUrl: 'components/connect-accounts/connect-accounts.html',
    bindings: {
      currentUser: '<'
    },
    controller: 'ConnectController'
  });

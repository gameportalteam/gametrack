'use strict';

angular.module('GameTrack.game')
  .factory('Game', ($resource) => {
    return $resource('/api/games/:id/:controller', {}, {
      getAll: {
        method: 'GET',
        isArray: true
      },
      getById: {
        method: 'GET'
      },
      getCommunitiesByGameId: {
        method: 'GET',
        isArray: true,
        params: {
          controller: 'communities'
        }
      },
      getProfilesByGameId: {
        method: 'GET',
        isArray: true,
        params: {
          controller: 'profiles'
        }
      }
    });
  });

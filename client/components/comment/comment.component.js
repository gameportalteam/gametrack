'use strict';

angular.module('GameTrack.comment')
  .component('comments', {
    templateUrl: 'components/comment/comment.html',
    bindings: {
      entity: '<',
      api: '<'
    },
    controller: 'CommentsController'
  });

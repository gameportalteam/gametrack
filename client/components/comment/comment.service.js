'use strict';

function CommentsResource($resource) {
  return $resource('/api/comments', {}, {
    create: {
      method: 'POST',
      isArray: false
    },
    getCommentsToId: {
      method: 'GET',
      isArray: true,
      url: '/api/:entity/:id/comments',
      params: {
        entity: '@entity',
        id: '@id'
      }
    }
  });
}

angular.module('GameTrack.comment')
  .factory('Comments', CommentsResource);

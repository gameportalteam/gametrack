'use strict';

function commentsController(appConfig, Comments, CreateGpModal, Auth, $scope, $uibModal) {
  this.action = 'comment';
  this.comments = [];
  this.currentUser = Auth.getCurrentUser();
  this.ava = this.currentUser.avatarurl || appConfig.defaultAvatar;
  this.newComment = {
    rootId: this.entity.entityId,
    targetEntityId: this.entity.entityId,
    gameId: this.entity.gameId,
    path: '',
    text: ''
  };
  const _getNewComments = () => {
    Comments.getCommentsToId({
      entity: this.api,
      id: this.entity.entityId
    }).$promise
      .then(res => {
        this.comments = res;
      });
  };
  _getNewComments();

  const _processCommentWithoutGp = comment => {
    return CreateGpModal.open(this.currentUser, this.entity.gameId, this.entity.entityId)
      .then(() => _createComment(comment));
  };

  const _createComment = comment => {
    comment = Object.assign({}, comment, { text: comment.text.replace(/\n/g, '<br>') });
    return Comments.create({
      entity: this.api,
      newComment: comment
    }).$promise
      .then(response => {
        if (response.reason === 'gp not exist') {
          return _processCommentWithoutGp(comment);
        }
        return _getNewComments();
      });
  };

  this.createComment = () => _createComment(this.newComment)
    .then(() => {
      this.newComment.text = '';
    });

  this.reply = comment => {
    return $uibModal.open({
      component: 'reply-modal',
      resolve: {
        targetAuthor: () => comment.authorName,
        targetComment: () => comment.text,
        onReply: () => text => {
          if (!text) return Promise.reject();
          const replyComment = Object.assign({}, this.newComment, {
            targetEntityId: comment.entityId,
            path: comment.path,
            text
          });
          return _createComment(replyComment);
        }
      }
    });
  };

  this.calcOffset = path => {
    const step = 3;
    const maxOffset = 50;
    const offset = (path.split('/').length - 2) * step;
    return Math.min(offset, maxOffset);
  };
}

angular.module('GameTrack')
  .controller('CommentsController', commentsController);

'use strict';

angular.module('GameTrack.comment')
  .component('replyModal', {
    templateUrl: 'components/comment/reply-modal/reply-modal.html',
    bindings: {
      resolve: '<',
      close: '&',
      dismiss: '&'
    },
    controller: function ReplyModalController() {
      const { onReply, targetAuthor, targetComment } = this.resolve;
      this.targetAuthor = targetAuthor;
      this.targetComment = targetComment;

      this.reply = text => {
        if (this.form.$invalid) { return; }
        onReply(text)
          .then(this.close)
          .catch((err) => {
            if (err && err.status === 401) return this.close();
            this.error = true;
          });
      };
    }
  });

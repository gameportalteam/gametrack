'use strict';

angular.module('GameTrack')
  .controller('CommunityCatalogController', function communityCatalogController(Community) {
    Community.getDomain().$promise
      .then((res) => {
        this.parentId = res[0]._id;
        return Community.getChildrens({ id: this.parentId }).$promise;
      })
      .then((res) => {
        this.communities = res;
      });
    this.click = (community) => {
      this.communities = Community.getChildrens({ id: community._id });
    };
  });

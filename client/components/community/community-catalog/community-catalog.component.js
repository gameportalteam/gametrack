'use strict';

angular.module('GameTrack')
  .component('communityCatalog', {
    templateUrl: 'components/community/community-catalog/community-catalog.html',
    controller: 'CommunityCatalogController'
  });

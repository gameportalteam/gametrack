'use strict';

angular.module('GameTrack.community')
  .factory('Community', ($resource) => {
    return $resource('/api/communities/:id/:controller', {
      id: '@_id'
    }, {
      getAll: {
        method: 'GET',
        isArray: true
      },
      getDomain: {
        method: 'GET',
        isArray: true,
        params: {
          id: 'domain'
        }
      },
      getChildrens: {
        method: 'GET',
        isArray: true,
        params: {
          controller: 'communities'
        }
      },
      getCommunity: {
        method: 'GET'
      },
      getGameProfiles: {
        url: '/api/communities/:id/profiles',
        method: 'GET',
        isArray: true,
      },
      addGameProfile: {
        url: 'api/communities/:id/addGameProfile/:profileId',
        method: 'PUT',
        params: {
          id: '@id',
          profileId: '@profileId'
        }
      }
    });
  });

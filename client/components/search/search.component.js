'use strict';

angular.module('GameTrack.search', ['GameTrack.constants'])
  .component('search', {
    templateUrl: 'components/search/search.html',
    controller: 'SearchController'
  });

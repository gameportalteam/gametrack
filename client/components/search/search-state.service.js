'use strict';

const searchState = () => {
  const defaultGame = {
    title: 'GT Accounts'
  };
  const defaultProfileUser = {
    name: ''
  };
  const allUsers = {
    name: 'All users'
  };
  const allProfiles = {
    name: 'All profiles'
  };
  return {
    game: Object.create(defaultGame),
    profileUser: Object.assign(defaultProfileUser),
    getGame() {
      return this.game;
    },
    setGame(game) {
      this.setProfileUser(null);
      this.game = game || Object.create(defaultGame);
      return this.game;
    },
    getProfileUser() {
      return this.profileUser;
    },
    setProfileUser(profileUser) {
      this.profileUser = profileUser || Object.assign(defaultProfileUser);
      return this.profileUser;
    },
    setAllProfiles() {
      this.profileUser = Object.assign(allProfiles);
    },
    setAllUsers() {
      this.profileUser = Object.assign(allUsers);
    }
  };
};

angular.module('GameTrack')
  .factory('SearchState', searchState);

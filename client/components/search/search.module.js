'use strict';

angular.module('GameTrack.search', [])
  .run(($templateCache) => {
    $templateCache.put('components/search/dropdown-template-game.html', 'components/search/dropdown-template-game.html');
    $templateCache.put('components/search/dropdown-template-gp-user.html', 'components/search/dropdown-template-gp-user.html');
  });

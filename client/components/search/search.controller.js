function searchController(Game, User, $scope, $state, $translate, appConfig, SearchState, Auth) {
  const placeholders = [
    'autocomplete.SELECT_PROFILE',
    'autocomplete.SELECT_USER'
  ];
  const profilesPholder = 0;
  const usersPholder = 1;
  const vm = this;
  this.game = SearchState.getGame();
  this.profileUser = SearchState.getProfileUser();
  this.gamePlaceholder = () => $translate.instant('autocomplete.SELECT_GAME');
  this.noResults = () => $translate.instant('autocomplete.NO_RESULTS');
  this.searching = () => $translate.instant('autocomplete.SEARCHING');
  this.getPlaceholder = () => {
    const index = (this.game && this.game.id) ? profilesPholder : usersPholder;
    return $translate.instant(placeholders[index]);
  };

  this.remoteApi = (str) => {
    if (!str) {
      return this.getRecentGames();
    }
    return Game.getAll({ game: str }).$promise
      .then(res => {
        return res;
      });
  };

  this.getRecentGames = () => {
    return Game.getAll({ game: '' }).$promise
      .then(res => {
        return res;
      });
  };

  this.formatGameResponse = (res) => {
    const allGames = {
      bottom: true,
      type: 'games',
      title: 'All games'
    };
    res.push(allGames);
    return res;
  };

  this.remoteProfileUserApi = (str) => {
    if (!str) {
      return this.getRecentProfilesUsers();
    }
    if (this.game && this.game.id) {
      return Game.getProfilesByGameId({
        id: this.game.id,
        name: str
      }).$promise;
    }
    return User.getAll({ name: str }).$promise;
  };

  this.getRecentProfilesUsers = () => {
    if (this.game && this.game.id) {
      return Game.getProfilesByGameId({
        id: this.game.id,
      }).$promise;
    }
    return User.getAll({ name: '' }).$promise;
  };


  function formatProfileUserResponse(res) {
    const selectedGameId = vm.game && vm.game.id ? vm.game.id : 0;
    vm.currentUser = Auth.getCurrentUser();
    const newProfile = {
      name: this.searchStr,
      gameId: selectedGameId,
      top: res.length,
      create: true,
      type: 'create'
    };
    const allProfiles = {
      bottom: true,
      type: 'profiles',
      name: 'All profiles',
    };
    const allUsers = {
      bottom: true,
      type: 'users',
      name: 'All users',
    };
    if (selectedGameId) {
      res.push(allProfiles);
      const uniqueNickname = res.reduce((p, c) => (c.name === this.searchStr ? p + 1 : p), 0);
      if (this.searchStr && !uniqueNickname) {
        res.unshift(newProfile);
      }
    } else {
      res.push(allUsers);
    }
    return res;
  }

  this.formatProfileUserResponse = formatProfileUserResponse;

  this.focusInGame = () => {
    $scope.$broadcast('angucomplete-alt:clearInput', 'search-game-autocomplete');
  };
  this.focusOutGame = () => {
    this.game = SearchState.getGame();
    $scope.$broadcast('angucomplete-alt:changeInput', 'search-game-autocomplete',
      this.game.title);
  };

  this.focusInProfileUser = () => {
    $scope.$broadcast('angucomplete-alt:clearInput', 'search-gp-user-autocomplete');
  };
  this.focusOutProfileUser = () => {
    this.profileUser = SearchState.getProfileUser();
    $scope.$broadcast('angucomplete-alt:changeInput', 'search-gp-user-autocomplete',
      this.profileUser.name);
  };

  $scope.$watch(() => SearchState.game, game => {
    this.game = game;
    $scope.$broadcast('angucomplete-alt:changeInput', 'search-game-autocomplete',
      this.game.title);
    $scope.$broadcast('angucomplete-alt:clearInput', 'search-gp-user-autocomplete');
  });

  $scope.$watch(() => SearchState.profileUser, profileUser => {
    this.profileUser = profileUser;
    $scope.$broadcast('angucomplete-alt:changeInput', 'search-gp-user-autocomplete',
      this.profileUser.name);
  });

  // Game selected
  $scope.$watch(() => this.game, newValue => {
    if (!newValue || !newValue.description) return;
    if (newValue.originalObject.type === 'games') {
      $state.go('games');
      return;
    }
    const gameId = this.game.description.id;
    $state.go('game.main', { gameId });
  });

  // Profile or User selected
  $scope.$watch(() => this.profileUser, newValue => {
    if (newValue && newValue.description) {
      if (newValue.originalObject.type === 'users') {
        return $state.go('users');
      }
      if (newValue.originalObject.type === 'profiles') {
        return $state.go('game.profiles');
      }
      if (newValue.originalObject.type === 'create') {
        return Auth.createProfile(newValue.originalObject.name, newValue.originalObject.gameId)
          .then(profile => {
            return $state.go('game.profile', {
              gameId: this.game.id,
              profileId: profile.id
            });
          });
      }
      const id = this.profileUser.description.id;
      if (this.game && this.game.id) {
        return $state.go('game.profile', {
          gameId: this.game.id,
          profileId: id
        });
      }
      return $state.go('user', { id });
    }
  });
}

angular.module('GameTrack.search')
  .controller('SearchController', searchController);

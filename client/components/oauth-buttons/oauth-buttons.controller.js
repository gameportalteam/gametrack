'use strict';

angular.module('GameTrack')
  .controller('OauthButtonsCtrl', function oauthButtonsCtrl($window) {
    this.loginOauth = function loginOauth(provider) {
      $window.location.href = `/auth/${provider}`;
    };
  });

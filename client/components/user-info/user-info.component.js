'use strict';

angular.module('GameTrack')
  .component('userInfo', {
    templateUrl: 'components/user-info/user-info.html',
    bindings: {
      userId: '='
    },
    controller: function userInfoController($scope, User) {
      this.user = {};
      $scope.$watch(() => this.userId, (newVal) => {
        if (newVal) {
          this.user = User.getById({ id: newVal });
          this.gameProfiles = User.getGameProfiles({ id: newVal });
        }
      });
    }
  });

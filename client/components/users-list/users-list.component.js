'use strict';

angular.module('GameTrack')
  .component('usersList', {
    templateUrl: 'components/users-list/users-list.html',
    bindings: {
      selectUser: '='
    },
    controller: function userListController(User) {
      this.users = User.getAll();
    }
  });

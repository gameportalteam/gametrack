'use strict';

angular.module('GameTrack.report')
  .controller('ReportController', function reportController(Auth, Reports) {
    this.fromId = Auth.getCurrentUser().id;
    this.action = +this.isPositive ? 'COMMEND' : 'COMPLAIN';

    if (this.fromId) {
      Reports.get().$promise
        .then(res => {
          this.title = `report.${this.action}`;
          this.variants = res[this.action];
        });
    }

    this.submit = (id) => {
      Reports.create({
        type: this.entityType,
        report: {
          fromId: this.fromId,
          toId: this.toId,
          reportId: id
        }
      });
    };
  });

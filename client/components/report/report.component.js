'use strict';

angular.module('GameTrack.report')
  .component('report', {
    templateUrl: 'components/report/report.html',
    bindings: {
      entityType: '<',
      isPositive: '@',
      toId: '<'
    },
    controller: 'ReportController'
  });

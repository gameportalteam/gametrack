'use strict';

angular.module('GameTrack.report')
  .factory('Reports', ($resource) => {
    return $resource('/api/reportslist', {}, {
      get: {
        method: 'GET',
        isArray: false,
        cache: true
      },
      create: {
        method: 'POST',
        url: '/api/reports'
      }
    });
  });

'use strict';

angular.module('GameTrack.top-n-profiles')
  .component('topNProfiles', {
    templateUrl: 'components/top-n-profiles/top-n-profiles.html',
    bindings: {
      entity: '<',
      api: '<',
      profilesCount: '<'
    },
    controller: function TopNProfilesController(TopNProfiles) {
      this.translationData = {
        count: this.profilesCount
      };
      this.profiles = [];
      this.getProfiles = () => {
        TopNProfiles.getTopNProfiles({
          entity: this.api,
          id: this.entity.id,
          count: this.profilesCount
        }).$promise
          .then(res => {
            this.profiles = res;
            return;
          });
      };
      this.getProfiles();
    }
  });

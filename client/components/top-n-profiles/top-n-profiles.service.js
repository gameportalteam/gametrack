'use strict';

angular.module('GameTrack.top-n-profiles')
  .factory('TopNProfiles', ($resource) => {
    return $resource('/api/:entity/:id/topProfiles', {}, {
      getTopNProfiles: {
        method: 'GET',
        isArray: true,
        params: {
          entity: '@entity',
          id: '@id'
        }
      }
    });
  });

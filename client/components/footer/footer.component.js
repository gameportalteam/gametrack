'use strict';

angular.module('GameTrack')
  .component('footer', {
    templateUrl: 'components/footer/footer.html'
  });

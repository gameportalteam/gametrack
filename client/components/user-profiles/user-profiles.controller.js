function groupByGame(profiles) {
  const result = {};
  for (const profile of profiles) {
    if (result[profile.game.title]) {
      result[profile.game.title].push(profile);
    } else result[profile.game.title] = [profile];
  }
  return result;
}

function userProfilesController($q, $translate, UserProfiles, GameProfiles) {
  this.primary = () => $translate.instant('users.PRIMARY');
  this.disabled = false;
  this.disable = () => {
    this.disabled = !this.disabled;
  };
  UserProfiles.getAll({ id: this.userId }).$promise
    .then(res => {
      if (!res.length) return;
      this.profiles = groupByGame(res);
    });
  this.setAsPrimary = (profileId, gameId) => {
    this.disable();
    GameProfiles.setAsPrimary({
      newPrimaryId: profileId,
      accountId: this.userId,
      gameId: gameId
    }).$promise
      .then(() => this.disable());
  };
}

angular.module('GameTrack.user-profiles')
  .controller('UserProfilesController', userProfilesController);

angular.module('GameTrack')
  .component('userProfiles', {
    templateUrl: 'components/user-profiles/user-profiles.html',
    bindings: {
      userId: '<'
    },
    controller: 'UserProfilesController'
  });


'use strict';

angular.module('GameTrack.user-profiles')
  .factory('UserProfiles', ($resource) => {
    return $resource('/api/users/:id/gameprofiles', {}, {
      getAll: {
        method: 'GET',
        isArray: true,
        params: { id: '@id' }
      }
    });
  });

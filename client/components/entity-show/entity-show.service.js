'use strict';

const entityShowResource = ($resource) => {
  return $resource('/api/entityShow', {}, {
    get: {
      method: 'GET',
      isArray: true
    }
  });
};

angular.module('GameTrack')
  .factory('EntityShow', entityShowResource);

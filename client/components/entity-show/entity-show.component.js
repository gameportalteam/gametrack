'use strict';

angular.module('GameTrack')
  .component('entityShow', {
    templateUrl: 'components/entity-show/entity-show.html',
    bindings: {
      entityType: '<',
      attributes: '<',
      searchField: '<',
      where: '<',
      order: '<'
    },
    controller: 'EntityShowController'
  });

'use strict';

angular.module('GameTrack')
  .controller('EntityShowController', function entityShowController(EntityShow) {
    this.data = [];
    this.orderString = `${this.order.fields[0]} ${this.order.order}`;
    this.searchQuery = '';
    this.params = {
      entityType: this.entityType,
      attributes: Object.keys(this.attributes),
      where: this.where || {},
      order: this.orderString,
      page: 1
    };

    this.get = (params) => {
      EntityShow.get(params).$promise
        .then(res => {
          this.data = res;
        });
    };
    this.get(this.params);

    this.search = () => {
      const query = {
        [this.searchField]: {
          $iLike: `%${this.searchQuery}%`
        }
      };
      const params = {
        entityType: this.params.entityType,
        attributes: this.params.attributes,
        where: angular.extend(this.params.where, query),
        order: this.orderString,
        page: 1
      };
      this.get(params);
    };

    this.changeOrder = (field) => {
      this.orderString = `"${field}" ${this.order.order}`;
      this.params.order = this.orderString;
      this.get(this.params);
    };

    this.getAvatar = (entity) => {
      return entity.avatarurl || 'http://placekitten.com/g/80/80';
    };
  });

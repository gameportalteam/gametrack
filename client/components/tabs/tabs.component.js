'use strict';

angular.module('GameTrack.tabs')
  .component('tabs', {
    templateUrl: 'components/tabs/tabs.html'
  });

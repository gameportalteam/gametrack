'use strict';

function likeController($state, Auth, Like, CreateGpModal, $scope) {
  const vm = this;

  vm.action = 'like';
  vm.likeType = {
    pos: 1,
    neg: -1
  };
  vm.gameId = this.entity.gameId;
  vm.currentUser = Auth.getCurrentUser();
  vm.likesCount = vm.entity.likesCount;
  vm.userLike = vm.entity.userLike || {
    targetEntityId: vm.entity.entityId,
    gameId: vm.entity.gameId
  };

  vm.setLike = (type) => {
    Like.set({
      targetEntityId: vm.userLike.targetEntityId,
      type,
      gameId: vm.entity.gameId
    }).$promise
      .then(response => {
        if (response.reason === 'gp not exist') {
          CreateGpModal.open(vm.currentUser, vm.gameId,
            vm.entity.entityId, vm.action);
          return;
        }
        vm.likesCount = response.likesCount;
        angular.extend(vm.userLike, response.userLike);
      });
  };

  vm.resetLike = (type) => {
    Like.reset({
      targetEntityId: vm.userLike.targetEntityId,
      type,
      gameId: vm.entity.gameId
    }).$promise
      .then(response => {
        vm.likesCount = response.likesCount;
        vm.userLike.id = undefined;
      });
  };

  vm.like = (type) => {
    vm.currentType = type;
    if (!vm.currentUser.id) {
      $state.go('login');
    }
    if (vm.userLike.id && vm.userLike.type === vm.currentType) {
      vm.resetLike(vm.currentType);
    } else {
      vm.setLike(vm.currentType);
    }
  };

  this.created = (event, gp) => {
    const ctrl = event.currentScope.$ctrl;
    ctrl.userLike.whoId = gp.id;
    if (ctrl.entity.entityId !== gp.targetEntityId
      || ctrl.action !== gp.action) {
      return;
    }
    ctrl.like(ctrl.currentType);
  };

  $scope.$on('gp.created', this.created);
}

angular.module('GameTrack')
  .controller('LikeController', likeController);

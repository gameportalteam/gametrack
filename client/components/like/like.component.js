'use strict';

angular.module('GameTrack')
  .component('like', {
    templateUrl: 'components/like/like.html',
    controller: 'LikeController',
    bindings: {
      entity: '='
    }
  });

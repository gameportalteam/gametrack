'use strict';

function LikeResource($resource) {
  return $resource('/api/likes/:controller', {}, {
    set: {
      method: 'POST',
      params: {
        controller: 'set'
      }
    },
    reset: {
      method: 'POST',
      params: {
        controller: 'reset'
      }
    }
  });
}

angular.module('GameTrack')
  .factory('Like', LikeResource);

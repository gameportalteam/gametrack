'use strict';

function settingsResource($resource) {
  return $resource('/api/:method/:action', {
    method: '@method',
    action: '@action'
  }, {
    saveData: { method: 'PUT' },
  });
}

angular.module('GameTrack.user-profiles')
  .factory('settingsService', settingsResource);

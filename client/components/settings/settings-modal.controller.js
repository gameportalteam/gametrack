'use strict';

function settingsModalController($uibModalInstance, settingsService, $state, settings) {
  this.settings = settings;
  settings.forEach(setting => {
    this[setting.name] = {
      value: '',
      errorMessage: '',
    };
  });

  this.saveData = () => {
    this.isLoading = true;
    this.errorMessage = '';
    const promises = [];
    settings.forEach(setting => {
      const newData = this[setting.name].value;
      if (!newData) return;

      const promise = settingsService.saveData({
        [setting.name]: newData,
        method: setting.method,
        action: setting.action,
      }).$promise;

      promises.push(promise);

      promise.then(null, (reason) => {
        const message = reason.data.message || 'ERRORMESSAGE';
        this[setting.name].errorMessage = `settings.${message}`;
      });
    });

    Promise.all(promises)
      .then(() => {
        this.isLoading = false;
        $uibModalInstance.close();
        $state.reload();
      }, () => {
        this.isLoading = false;
      });
  };

  this.cancel = () => {
    $uibModalInstance.dismiss();
  };
}

angular.module('GameTrack')
  .controller('settingsModalController', settingsModalController);

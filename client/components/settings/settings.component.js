'use strict';

angular.module('GameTrack')
  .component('settings', {
    templateUrl: 'components/settings/settings.html',
    controller: 'settingsController',
    controllerAs: 'st',
    bindings: {
      settings: '<'
    },
  });

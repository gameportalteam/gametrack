'use strict';

function settingsController($uibModal) {
  this.edit = (size) => {
    $uibModal.open({
      animation: true,
      size: size,
      templateUrl: 'components/settings/settings-modal.html',
      controller: 'settingsModalController',
      controllerAs: 'stm',
      resolve: {
        settings: () => {
          return Promise.resolve(this.settings);
        }
      }
    });
  };
}

angular.module('GameTrack')
  .controller('settingsController', settingsController);

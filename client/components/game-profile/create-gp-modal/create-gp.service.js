'use strict';

angular.module('GameTrack.game-profile')
  .factory('CreateGpModal', ($uibModal) => {
    const open = (currentUser, currentGameId, entityId, action, size) => {
      return $uibModal.open({
        animation: true,
        size: size,
        templateUrl: 'components/game-profile/create-gp-modal/create-gp-modal.html',
        controller: 'CreateGpModalController as $ctrl',
        resolve: {
          currentUser: currentUser,
          currentGameId: currentGameId,
          entityId: entityId,
          action: () => action
        }
      }).result;
    };

    return {
      open
    };
  });

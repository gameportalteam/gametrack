'use strict';

angular.module('GameTrack.game-profile')
  .controller('CreateGpModalController', function CreateGpModalController($uibModalInstance, appConfig,
                                                   $translate, User, currentUser, currentGameId, entityId, action, $rootScope, Auth,
                                                   GameProfiles) {
    this.remoteApi = (request) => {
      this.input = request;
      return GameProfiles.getFree({
        name: request
      }).$promise;
    };

    this.noResults = () => $translate.instant('autocomplete.NO_RESULTS');
    this.searching = () => $translate.instant('autocomplete.SEARCHING');

    this.requestOwnership = () => (
      this.gamer
        ? User.addGameProfile({
          id: currentUser.id,
          gameProfileId: this.gamer.originalObject.id
        }).$promise
        : Auth.createProfile(this.input, currentGameId)
    )
    .then(gp => {
      if (action) { gp.action = action; }
      gp.targetEntityId = entityId;
      Auth.getCurrentProfile(gp.gameId);
      $rootScope.$broadcast('gp.created', gp);
      return $uibModalInstance.close({ $value: gp });
    })
    .catch(this.cancel);

    this.cancel = () => {
      $uibModalInstance.dismiss();
    };
  });

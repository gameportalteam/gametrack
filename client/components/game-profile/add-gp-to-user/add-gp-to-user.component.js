'use strict';

angular.module('GameTrack.game-profile')
  .component('addGpToUser', {
    templateUrl: 'components/game-profile/add-gp-to-user/add-gp-to-user.html',
    controller: 'AddGpToUserController',
    bindings: {
      profile: '=',
      gamer: '=',
      user: '='
    }
  });

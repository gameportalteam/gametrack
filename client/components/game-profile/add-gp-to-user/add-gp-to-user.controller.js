'use strict';

function AddGpToUserController($uibModal, User, Auth, $filter) {
  const $translate = $filter('translate');

  this.currentUser = Auth.getCurrentUser();
  this.profile_ownership = $translate('game-profile.TAKE_OWNERSHIP');
  this.profile_myprofile = $translate('game-profile.ITS_MY_PROFILE');

// request to get Gamer ownership
  this.takeOwnership = () =>
    User.addGameProfile({
      id: this.currentUser.id,
      gameProfileId: this.gamer.id
    }).$promise
      .then(newProfile => {
        return User.getById({ id: newProfile.accountId }).$promise;
      })
      .then(user => {
        this.gamer.user = user;
      });

// request to get User ownership
  this.openRequestModal = (size) => {
    $uibModal.open({
      animation: true,
      size: size,
      templateUrl: 'components/game-profile/add-gp-to-user/add-gp-to-user-modal.html',
      controller: 'AddGpToUserModalController as $ctrl',
      resolve: {
        user: this.currentUser
      }
    });
  };
}

angular.module('GameTrack.game-profile')
  .controller('AddGpToUserController', AddGpToUserController);

'use strict';

function addGpToUserModalController($uibModalInstance, GameProfiles, user) {
  this.requestOwnership = () => {
    GameProfiles.requestOwnership({ id: user.id });
    $uibModalInstance.close();
  };

  this.cancel = () => {
    $uibModalInstance.dismiss();
  };
}

angular.module('GameTrack.game-profile')
  .controller('AddGpToUserModalController', addGpToUserModalController);

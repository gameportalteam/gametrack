'use strict';

angular.module('GameTrack.game-profile')
  .component('gameProfiles', {
    templateUrl: 'components/game-profile/game-profiles/game-profiles.html',
    controller: 'GameProfilesController',
    bindings: {
      profiles: '<',
      game: '<'
    }
  });

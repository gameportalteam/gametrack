'use strict';

function GameProfilesResource($resource) {
  return $resource('/api/gameProfiles/:controller', {
    id: '@_id'
  }, {
    getFree: {
      method: 'GET',
      isArray: true,
      params: {
        controller: 'free'
      }
    },
    create: {
      method: 'POST',
      isArray: false
    },
    getById: {
      method: 'GET',
      url: '/api/gameProfiles/:id',
      params: {
        id: '@id'
      }
    },
    requestOwnership: {
      url: '/api/gameProfiles/:id/requestOwnership',
      method: 'POST',
      params: {
        id: '@id'
      }
    },
    getAll: {
      method: 'GET',
      isArray: true
    },
    setAsPrimary: {
      method: 'POST',
      url: '/api/gameProfiles/setAsPrimary'
    }
  });
}

angular.module('GameTrack.game-profile')
  .factory('GameProfiles', GameProfilesResource);


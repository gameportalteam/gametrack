'use strict';

angular.module('GameTrack.game-profile')
  .component('gameProfile', {
    templateUrl: 'components/game-profile/game-profile/game-profile.html',
    controller: 'GameProfileController',
    bindings: {
      profile: '='
    }
  });

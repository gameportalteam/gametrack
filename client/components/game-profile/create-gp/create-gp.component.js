'use strict';

angular.module('GameTrack')
  .component('createGp', {
    templateUrl: 'components/game-profile/create-gp/create-gp.html',
    controller: function createGpController(Auth, Community, GameProfiles) {
      Community.getDomain().$promise
        .then((res) => {
          this.parentId = res[0]._id;
          return Community.getChildrens({ id: this.parentId }).$promise;
        })
        .then((res) => {
          this.communities = res;
        });
      this.currentUser = Auth.getCurrentUser();
      this.gameProfile = {
        name: '',
        rootCommunityId: ''
      };

      this.createGameProfile = () => {
        GameProfiles.create(this.gameProfile);
        this.gameProfile = {
          name: '',
          rootCommunityId: ''
        };
      };
    }
  });

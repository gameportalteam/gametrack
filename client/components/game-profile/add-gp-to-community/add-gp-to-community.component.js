'use strict';

angular.module('GameTrack.game-profile')
  .component('addGpToCommunity', {
    templateUrl: 'components/game-profile/add-gp-to-community/add-gp-to-community.html',
    controller: 'AddGpToCommunityController'
  });

'use strict';

angular.module('GameTrack.game-profile')
  .controller('AddGpToCommunityController', function addGpToCommunityController($scope, GameProfiles, Community) {
    this.selectedCommunity = null;
    this.gameProfiles = [];
    this.allGameProfiles = GameProfiles.getFree();
    this.communities = Community.getAll();

    this.addGameProfile = () => {
      Community.addGameProfile({
        id: this.selectedCommunity._id,
        profileId: this.gameProfile.originalObject._id
      });
    };

    $scope.$watch(() => this.selectedCommunity, newValue => {
      $scope.$broadcast('angucomplete-alt:clearInput', 'add-gp-to-community-autocomplete');
      if (!newValue) {
        return;
      }
      const rootId = getRootOfCommunity(newValue);
      this.gameProfiles = this.allGameProfiles
        .filter(x => x.rootCommunityId === rootId);
    });

    function getRootOfCommunity(community) {
      const pathArray = community.path.split(' ');
      const rootCommunityId = pathArray[1] || community._id;
      return rootCommunityId;
    }
  });

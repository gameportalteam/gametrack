'use strict';

angular.module('GameTrack.comment')
  .component('headerContent', {
    templateUrl: 'components/header-content/header-content.html',
    bindings: {
      title: '@',
      preTitle: '@',
      description: '@',
      path: '<',
      game: '<',
      gamer: '<',
      entity: '<',
      karma: '<',
      image: '@',
      imagestyle: '@',
      settings: '<'
    },
    controller: function HeaderController() {
      this.karmaExist = this.karma && this.karma.length;
      this.showGameProfile = (this.gamer && this.gamer.user.name.length) ? true : undefined;
      this.showUserProfile = this.entity && (this.entity.entityType === 'account') ? true : undefined;
    },
    controllerAs: 'hc'
  });

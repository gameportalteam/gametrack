'use strict';

angular.module('GameTrack')
  .component(
    'headerContentGameProfile',
    { templateUrl: 'components/header-content/header-content-gameprofile/header-content-gameprofile.html',
      bindings: {
        gametitle: '<',
        preTitle: '<',
        description: '<',
        path: '<',
        game: '<',
        gamer: '<',
        karma: '<',
        image: '<',
        imagestyle: '<'

      },
      controller: function HeaderGameProfileController() {
        // this.title=title;
        // this.gameProfile = gameProfile;
      },
      controllerAs: 'hcgp'
    }
  );

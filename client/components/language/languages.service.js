'use strict';

angular.module('GameTrack')
  .factory('Languages', ($resource) => {
    return $resource('/components/language/languages.json', {}, {
      get: {
        method: 'GET',
        isArray: 'true'
      }
    });
  });

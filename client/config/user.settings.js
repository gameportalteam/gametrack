module.exports = [
  {
    name: 'avatarUrl',
    type: 'text',
    description: 'settings.AVATARPLACEHOLDER',
    pattern: '.*\\.(jpeg|jpg|gif|png|JPEG|JPG|GIF|PNG)$',
    title: 'settings.AVATARTITLE',
    method: 'users',
    action: 'saveAvatarUrl',
  },
  {
    name: 'name',
    type: 'text',
    description: 'settings.NAMEPLACEHOLDER',
    pattern: '^[^0-9]\\w{3,20}$',
    title: 'settings.NAMETITLE',
    method: 'users',
    action: 'saveName',
  }
];

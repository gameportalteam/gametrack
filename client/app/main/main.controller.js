'use strict';

class MainController {

  selectUser(user) {
    this.selectedUser = user;
  }

  constructor($http, Auth) {
    this.awesomeThings = [];
    this.selectedUser = {};

    this.currentUser = Auth.getCurrentUser();
  }
}

angular.module('GameTrack')
  .controller('MainController', MainController);


'use strict';

angular.module('GameTrack')
  .config(($stateProvider) => {
    $stateProvider
      .state('oldmain', {
        url: '/oldMain',
        templateUrl: 'app/main/main.html',
        controller: 'MainController',
        controllerAs: 'main'
      });
  });

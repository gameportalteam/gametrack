'use strict';

angular.module('GameTrack')
  .controller('GameProfilePageController', function GameProfilePageController(appConfig, gameProfile, user, game, $translate) {
    gameProfile.user = user;
    this.game = game;
    this.gamer = gameProfile;
    this.gameProfile = gameProfile;
    this.description = $translate.instant('game-profiles.GAMER_PAGE');
    this.api = 'gameProfiles';
    this.username = user.name;
    this.imagestyle = 'avatar lg';
    this.defaultAvatar = appConfig.defaultAvatar;
  });

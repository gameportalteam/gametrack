'use strict';

angular.module('GameTrack')
  .config(($stateProvider) => {
    $stateProvider
      .state('game.profiles', {
        url: '/profiles',
        templateUrl: 'app/game-profiles/game-profiles.html',
        controller: 'GameProfilesPageController as $ctrl',
        resolve: {
          profiles: ($stateParams, Game) => {
            return Game.getProfilesByGameId({ id: $stateParams.gameId }).$promise;
          },
          searchState: (game, SearchState) => {
            return SearchState.setAllProfiles();
          }
        }
      })
      .state('game.profile', {
        url: '/profiles/{profileId:int}',
        // templateUrl: 'app/errors/maintenance.html'
        templateUrl: 'app/game-profiles/game-profile/game-profile-page.html',
        controller: 'GameProfilePageController as $ctrl',
        resolve: {
          gameProfile: ($state, $stateParams, GameProfiles, SearchState) =>
            GameProfiles.getById({ id: $stateParams.profileId })
              .$promise
              .then(profile => {
                if (profile.gameId !== +$stateParams.gameId) $state.go('404-profile');
                return SearchState.setProfileUser(profile);
              })
              .catch(() => $state.go('404')),

          user: (gameProfile, User) => {
            return gameProfile.accountId ?
              User.getById({ id: gameProfile.accountId }).$promise :
              null;
          }
        }
      })
      .state('game.community.profiles', {
        url: '/profiles',
        templateUrl: 'app/game-profiles/game-profiles.html',
        controller: 'GameProfilesPageController as $ctrl',
        resolve: {
          profiles: ($stateParams, Community) =>
            Community.getGameProfiles({ id: $stateParams.communityId }).$promise
        }
      });
  });

'use strict';

angular.module('GameTrack')
  .controller('GameProfilesPageController', function GameProfilesPageController(profiles, game) {
    this.profiles = profiles;
    this.game = game;
  });

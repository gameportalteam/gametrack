'use strict';

function adminController(users) {
  this.users = users;
  this.description = 'The delete user and user index api routes are restricted to users with the "admin" role.';

  this.delete = (user) => {
    user.$remove();
    this.users.splice(this.users.indexOf(user), 1);
  };
}

angular.module('GameTrack.admin')
  .controller('AdminController', adminController);

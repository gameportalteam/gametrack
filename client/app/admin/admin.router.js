'use strict';

angular.module('GameTrack.admin')
  .config(($stateProvider) => {
    $stateProvider
      .state('admin', {
        url: '/admin',
        templateUrl: 'app/admin/admin.html',
        controller: 'AdminController',
        controllerAs: 'admin',
        resolve: {
          users: (User, $state) => {
            return User.query({ controller: 'admin' }).$promise
              .catch(() => $state.go('404'));
          }
        }
      });
  });

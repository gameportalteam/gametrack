'use strict';

angular.module('GameTrack.admin', [
  'GameTrack.auth',
  'ui.router'
]);

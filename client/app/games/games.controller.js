'use strict';

function GamesController(appConfig, games, $translate) {
  this.title = $translate.instant('copyright.TITLE');
  this.description = $translate.instant('game.LIST');
  this.games = games;
  this.defaultLogo = appConfig.defaultLogo;
  this.imagestyle = 'avatar lg';
}

angular.module('GameTrack')
  .controller('GamesController', GamesController);


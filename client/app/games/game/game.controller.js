'use strict';

angular.module('GameTrack')
  .controller('GameController', function GameController($stateParams, game, currentGP, $state, $translate) {
    this.description = $translate.instant('game.GAME');
    this.game = game;
    this.api = 'games';
    this.imagestyle = 'gamelogo lg';
    this.currentGP = currentGP;
    this.gameId = $stateParams.gameId;
    this.profilesCount = 5;
    this.showGame = () => {
      const state = $state.$current.name;
      return !(state.indexOf('.community') > -1
        || (state.indexOf('.profile') > -1 && state.indexOf('.profiles') === -1));
    };
    this.showTabs = () => {
      const state = $state.$current.name;
      return !(state.indexOf('.community') > -1
        || (state.indexOf('.profile') > -1 && state.indexOf('.profiles') === -1));
    };
  });

angular.module('GameTrack')
  .controller('GameComController', function GameComController(game, currentGP) {
    this.entity = game;
    this.currentGP = currentGP;
    this.api = 'games';
  });

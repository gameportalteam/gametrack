'use strict';

angular.module('GameTrack')
  .config(($stateProvider) => {
    $stateProvider
      .state('games', {
        url: '/games',
        templateUrl: 'app/games/games.html',
        controller: 'GamesController as $ctrl',
        resolve: {
          searchState: (SearchState) => SearchState.setGame(null),
          games: (Game) => Game.getAll().$promise
        }
      })
      .state('game', {
        abstract: true,
        url: '/games/{gameId:int}',
        templateUrl: 'app/games/game/game.html',
        controller: 'GameController as $ctrl',
        resolve: {
          game: ($state, $stateParams, Game, SearchState) => {
            return Game.getById({ id: $stateParams.gameId }).$promise
              .then(game => {
                SearchState.setGame(game);
                return game;
              })
              .catch(() => {
                $state.go('404');
                return;
              });
          },
          currentGP: (Auth, game) => {
            return Auth.getCurrentProfile(game.id)
              .then(gp => gp);
          }
        }
      })
      .state('game.main', {
        url: '',
        templateUrl: 'app/games/game/game.main.html'
      })
      .state('game.comments', {
        url: '/comments',
        // templateUrl: 'app/errors/maintenance.html'
        templateUrl: 'app/comments/comments.html',
        controller: 'GameComController as $ctrl'
      });
  });

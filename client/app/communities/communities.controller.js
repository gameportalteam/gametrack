'use strict';

angular.module('GameTrack')
  .controller('CommunitiesController', function CommunitiesController($stateParams, game, communities) {
    this.communities = communities;
    this.game = game;
    this.gameId = $stateParams.gameId;
    this.description = 'List of communities';
    this.api = 'games';
    this.profilesCount = 5;

    this.entityType = 'community';
    this.attributes = {
      creatorId: 'creatorId',
      title: 'title',
      gameId: 'gameId'
    };
    this.searchField = 'title';
    this.where = {
      gameId: this.game.id
    };
    this.order = {
      fields: ['title'],
      order: 'ASC'
    };
  });

angular.module('GameTrack')
  .controller('ComntyComController', function CommunitiesController(community, currentGP) {
    this.entity = community;
    this.currentGP = currentGP;
    this.api = 'communities';
  });

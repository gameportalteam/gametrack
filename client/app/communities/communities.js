'use strict';

angular.module('GameTrack')
  .config(($stateProvider) => {
    $stateProvider
      .state('game.communities', {
        url: '/communities',
        // templateUrl: 'app/errors/maintenance.html'
        templateUrl: 'app/communities/communities.html',
        controller: 'CommunitiesController as $ctrl',
        resolve: {
          communities: ($stateParams, Game) =>
            Game.getCommunitiesByGameId({ id: $stateParams.gameId }).$promise
        }
      })
      .state('game.community', {
        abstract: true,
        url: '/communities/{communityId:int}',
        templateUrl: 'app/communities/community/community.html',
        controller: 'CommunityController as $ctrl',
        resolve: {
          community: ($state, $stateParams, Community) =>
            Community.getCommunity({ id: $stateParams.communityId }).$promise
              .then(community => {
                if (community.gameId !== +$stateParams.gameId) $state.go('404-community');
                return community;
              })
              .catch(() => $state.go('404'))
        }
      })
      .state('game.community.main', {
        url: '',
        // templateUrl: 'app/errors/maintenance.html'
        templateUrl: 'app/communities/community/community.main.html'
      })
      .state('game.community.communities', {
        url: '/communities',
        // templateUrl: 'app/errors/maintenance.html'
        templateUrl: 'app/communities/communities.html',
        controller: 'CommunitiesController as $ctrl',
        resolve: {
          communities: ($stateParams, Community) =>
            Community.getChildrens({ id: $stateParams.communityId }).$promise
        }
      })
      .state('game.community.comments', {
        url: '/comments',
        templateUrl: 'app/comments/comments.html',
        controller: 'ComntyComController as $ctrl'
      });
  });

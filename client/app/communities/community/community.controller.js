'use strict';

angular.module('GameTrack')
  .controller('CommunityController', function CommunityController(community, appConfig) {
    this.community = community;
    this.profilesCount = 5;
    this.api = 'communities';
    this.description = 'Community';
    this.defaultAvatar = appConfig.defaultCommunity;
    this.imagestyle = 'avatar lg';
  });

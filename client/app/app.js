'use strict';

angular.module('GameTrack', [
  'GameTrack.auth',
  'GameTrack.admin',
  'GameTrack.game-profile',
  'GameTrack.constants',
  'GameTrack.community',
  'GameTrack.comment',
  'GameTrack.search',
  'GameTrack.game',
  'GameTrack.top-n-profiles',
  'GameTrack.report',
  'GameTrack.user-profiles',
  'GameTrack.tabs',
  'GameTrack.errors',
  'ngCookies',
  'ngResource',
  'ngSanitize',
  'ngAnimate',
  'angucomplete-alt',
  'pascalprecht.translate',
  'ui.router',
  'validation.match',
  'ui.bootstrap'
])
  .config(
    (
      $urlRouterProvider,
      $locationProvider,
      $translateProvider,
      $urlMatcherFactoryProvider
    ) => {
      $urlRouterProvider
        .otherwise('/');

      $urlMatcherFactoryProvider.caseInsensitive(true);
      $urlMatcherFactoryProvider.strictMode(false);

      $translateProvider.useStaticFilesLoader({
        prefix: 'components/l10n/',
        suffix: '.json'
      });
      $translateProvider.preferredLanguage('en-US'); // $translate.use('ru-RU') to switch language at runtime
      $translateProvider.useSanitizeValueStrategy('escape');

      $locationProvider.html5Mode(true);
    }
  );

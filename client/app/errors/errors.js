'use strict';

angular.module('GameTrack.errors')
  .config(($stateProvider) => {
    $stateProvider
      .state('404', {
        views: {
          '@': {
            templateUrl: 'app/errors/404.html'
          }
        }
      })
      .state('404-profile', {
        views: {
          '@': {
            templateUrl: 'app/errors/profile-not-found.html',
            controller: 'ErrorsController as $ctrl'
          }
        }
      })
      .state('404-community', {
        views: {
          '@': {
            templateUrl: 'app/errors/community-not-found.html',
            controller: 'ErrorsController as $ctrl'
          }
        }
      });
  });

'use strict';

function errorsController($location) {
  [this.gameId] = $location.$$url.match(/\d(?=\/)/);
}

angular.module('GameTrack.errors')
  .controller('ErrorsController', errorsController);

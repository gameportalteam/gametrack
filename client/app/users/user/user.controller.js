'use strict';

angular.module('GameTrack')
  .controller('UserController', function UserController(Auth, appConfig, user, $translate, UserSettings) {
    this.currentUser = Auth.getCurrentUser();
    this.user = user;
    this.description = 'User page';
    this.description = $translate.instant('users.USER_PAGE');
    this.api = 'users';
    this.imagestyle = 'avatar lg';
    this.defaultAvatar = appConfig.defaultAvatar;

    this.settings = UserSettings;
  });

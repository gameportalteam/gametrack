'use strict';

angular.module('GameTrack')
  .config(($stateProvider) => {
    $stateProvider
      .state('users', {
        url: '/users',
        templateUrl: 'app/users/users.html',
        controller: 'UsersController',
        controllerAs: '$ctrl',
        resolve: {
          searchState: (SearchState) => {
            SearchState.setAllUsers();
          },
          users: (User) => User.getAll().$promise
        }
      })
      .state('user', {
        url: '/users/{id:int}',
        views: {
          '@': {
            // templateUrl: 'app/errors/maintenance.html'
            templateUrl: 'app/users/user/user.html',
            controller: 'UserController',
            controllerAs: '$ctrl'
          }
        },
        resolve: {
          user: ($state, $stateParams, User, SearchState) => User.getById({ id: $stateParams.id }).$promise
            .then(user => {
              return SearchState.setProfileUser(user);
            })
            .catch(() => $state.go('404'))
        }
      });
  });

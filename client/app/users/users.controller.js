'use strict';

function UsersController(appConfig, users, $translate) {
  this.title = $translate.instant('copyright.TITLE');
  this.description = $translate.instant('users.PROFILES');
  this.users = users;
  this.defaultLogo = appConfig.defaultLogo;
  this.imagestyle = 'avatar lg';
}

angular.module('GameTrack')
  .controller('UsersController', UsersController);

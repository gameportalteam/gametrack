'use strict';

function loginController(Auth, $state) {
  this.user = {};
  this.errors = {};
  this.submitted = false;

  this.login = (form) => {
    this.submitted = true;

    if (form.$valid) {
      Auth.login({
        email: this.user.email,
        password: this.user.password
      })
        .then(() => {
          // Logged in, redirect
          $state.go($state.current.referrer || 'home', $state.current.params);
        })
        .catch(err => {
          this.errors.other = err.message;
        });
    }
  };
}

angular.module('GameTrack')
  .controller('LoginController', loginController);

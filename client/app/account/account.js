'use strict';

angular.module('GameTrack')
  .config(($stateProvider) => {
    $stateProvider
      .state('login', {
        url: '/login',
        templateUrl: 'app/account/enter/enter.html',
        controller: 'EnterController',
        controllerAs: 'vm'
      })
      .state('logout', {
        url: '/logout?referrer',
        template: '',
        controller: ($state, Auth) => {
          const referrer = $state.params.referrer ||
            $state.current.referrer ||
            'home';
          Auth.logout();
          $state.go(referrer, $state.current.params);
        }
      })
      .state('signup', {
        url: '/signup',
        templateUrl: 'app/account/signup/signup.html',
        controller: 'SignupController',
        controllerAs: 'vm'
      })
      .state('settings', {
        url: '/settings',
        templateUrl: 'app/account/settings/settings.html',
        controller: 'SettingsController',
        controllerAs: 'vm',
        authenticate: true
      });
  })
  .run(($rootScope) => {
    $rootScope.$on('$stateChangeStart', (event, toState, toParams, fromState, fromParams) => {
      if (fromState && fromState.name) {
        const canRedirectAfterLogout = (toState.name === 'logout' && !fromState.authenticate);
        if (canRedirectAfterLogout || toState.name === 'login') {
          toState.referrer = fromState.name;
        }
        if (fromParams) {
          toState.params = fromParams;
        }
      }
    });
  });

'use strict';

function enterController(Auth, $state, $window) {
  this.user = {};
  this.errors = {};
  this.submitted = false;
  this.isLocalLogin = false;

  this.login = (enterForm) => {
    this.submitted = true;

    if (enterForm.$valid) {
      Auth.login({
        email: this.user.email,
        password: this.user.password
      })
        .then(() => $state.go($state.current.referrer || 'home', $state.current.params))
        .catch(err => {
          this.errors.other = err.message;
        });
    }
  };

  this.loginOauth = (provider) => {
    $window.location.href = `/auth/${provider}`;
  };
}

angular.module('GameTrack')
  .controller('EnterController', enterController);

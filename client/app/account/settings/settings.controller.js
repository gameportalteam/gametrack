'use strict';

function settingsController($filter, Auth) {
  const $translate = $filter('translate');

  this.errors = {};
  this.submitted = false;
  this.user = Auth.getCurrentUser();

  this.changePassword = (form) => {
    this.submitted = true;

    if (form.$valid) {
      Auth.changePassword(this.user.oldPassword, this.user.newPassword)
        .then(() => {
          this.message = $translate('settings.PASSWORD_CHANGED');
        })
        .catch(() => {
          form.password.$setValidity('mongoose', false);
          this.errors.other = $translate('settings.INCORRECT_PASSWORD');
          this.message = '';
        });
    }
  };
}

angular.module('GameTrack')
  .controller('SettingsController', settingsController);

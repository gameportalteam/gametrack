'use strict';

angular.module('GameTrack')
  .config(($stateProvider) => {
    $stateProvider
      .state('home', {
        url: '/',
        templateUrl: 'app/home/home.html',
        controller: 'HomeController as $ctrl',
        resolve: {
          game: (SearchState) => {
            SearchState.setProfileUser(null);
            return SearchState.setGame(null);
          }
        }
      });
  });

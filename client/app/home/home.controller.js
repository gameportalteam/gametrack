'use strict';

angular.module('GameTrack')
  .controller('HomeController', function HomeController() {
    this.entityType = 'user';
    this.attributes = {
      avatarurl: 'avatarurl',
      name: 'name'
    };
    this.searchField = 'name';
    this.order = {
      fields: ['name', 'email'],
      order: 'ASC',
    };
  });

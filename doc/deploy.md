Steps of the deploy process:

1. Prepare server with docker

To make deploy process as easy as possible we use ready docker images
to run docker. So at first step we need to install it. Remember that docker
will be easy installed only on linux.

Install ubuntu server 14.04

sudo apt-get update
sudo apt-get

Install docker


2. Database deployment

To install and run mongo database use prepared container from the docker HUB.

docker run -p 27017:27017 --name gametrack-docker -d mongo


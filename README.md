# GameTrack

This project was generated with the [Angular Full-Stack Generator](https://github.com/DaftMonk/generator-angular-fullstack) version 3.3.0.

## Getting Started

### Prerequisites

- [Git](https://git-scm.com/)
- [Node.js and npm](nodejs.org) Node ^4.2.3, npm ^2.14.7
- [Bower](bower.io) (`npm install --global bower`)
- [Ruby](https://www.ruby-lang.org) and then `gem install sass`
- [Gulp](http://gulpjs.com/) (`npm install --global gulp`)
- [PostgreSQL](https://www.postgresql.org/)


### Developing

1. Run `npm install` to install server dependencies.

2. Run `bower install` to install front-end dependencies.

3. If you do not have a database yet, you need:

    1. Open psql in your PostgreSQL setup folder

    2. Create new user with `CREATE USER gametrack_user WITH PASSWORD 'gMtRcKaEa42';`

    3. Create new db with `CREATE DATABASE gametrack_db;`

4. Run `gulp serve` to start the development server. It should automatically open the client in your browser when ready.

## Build & development

Run `gulp build` for building and `gulp serve` for preview.

## Testing

Running `npm test` will run the unit tests with karma.

## Migration

1. Modify necessary model(models)

2. Run `node ../node_modules/sequelize-cli/bin/sequelize migration:create --name <migration name>` under `database` folder.
   NOTE: name isn't necessary.

3. Set function with model changes in `up` property and backward in `down` property.
Your can use only this [functions](http://docs.sequelizejs.com/en/latest/docs/migrations/#functions).

4. Commit migration file(files).

5. Run migration at remote server `node ../node_modules/sequelize-cli/bin/sequelize db:migrate`.
   NOTE: sequelize run migration only once.

6. Use `node ../node_modules/sequelize-cli/bin/sequelize db:seed:all --seeders-path seeders\production\` to feed DB with fake/default data

